This MATLAB routine serves to demonstrate examples presented in: O. Rokos, R. H. J. Peerlings, J. Zeman, L. A. A. Beex. An adaptive variational Quasicontinuum methodology for lattice networks with localized damage. https://arxiv.org/abs/1604.04754.

For a more detailed description of this implementation see /guide/guide.pdf.