function [r2,Niter,lambda] = minimize_r_QC(atoms,samplingatoms,bonds,r1,...
    Rp,DBCIndicesQC,DBCValuesQC,F,FreeIndicesQC,...
    R0QC,z1,C,Phi,TOL_r,Larc,lambda,maxNumThreads)

% Solve for x - a vector of free degrees of freedom, using standard Newton algorithm
lhs = (Phi'*Phi)\(Phi'*[r1,Rp]);
RQC = lhs(:,1); % previous iteration configuration, relaxed
RQCp = lhs(:,2); % previous time-step configuration, relaxed
x = RQC(FreeIndicesQC);
F = F(FreeIndicesQC);
C = C(FreeIndicesQC);
eps_r = 1+TOL_r;
Niter = 0;
while eps_r > TOL_r
    Niter = Niter+1;
    [~,G,H,H_DBC] = grad_hess_QC(atoms,samplingatoms,bonds,x,DBCIndicesQC,...
        lambda*DBCValuesQC,FreeIndicesQC,R0QC,z1,Phi,maxNumThreads);
    
    % Compute individual increments
    lhs = -H\[G-lambda*F,-F+H_DBC*DBCValuesQC];
    dxt = lhs(:,1);
    dxF = lhs(:,2);
    
    % Parameters for indirect displacement control method
    dlambda = (Larc-C'*(x-RQCp(FreeIndicesQC)+dxt))/(C'*dxF);
    
    % Construct the increment
    dx = dxt+dlambda*dxF;
    lambda = lambda+dlambda;
    x = x+dx;
    
    % Update the error
    eps_r = norm(dx)/norm(x-R0QC(FreeIndicesQC))+...
        norm(G-lambda*F);
end

% Reconstruct converged r2- and rQC-vector from x
rQC = zeros(size(R0QC,1),1);
rQC(DBCIndicesQC) = R0QC(DBCIndicesQC)+lambda*DBCValuesQC;
rQC(FreeIndicesQC) = x; % QC displacement vector reconstruction
r2 = Phi*rQC; % full r reconstruction

end
