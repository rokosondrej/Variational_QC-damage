function [p,t,C,bonds] = initialize_mesh_4(atoms,bonds,SizeX,SizeY,...
    NotchX,NotchY,SizeS1,SizeS2,dSize,R0,Npad,HMax,PotentialP,TOL_g)

% Build regular mesh
edgeSize2 = dSize;
failure = 1;
while failure
    if mod(2*SizeX,edgeSize2) == 0 && mod(2*SizeY,edgeSize2) == 0
        if edgeSize2*2 > sqrt(2)*HMax
            failure = 0;
        else
            edgeSize1 = edgeSize2;
            edgeSize2 = edgeSize1*2;
        end
    else
        failure = 0;
    end
end
edgeSize = edgeSize1;

% Build p
Xcoord = zeros(2*SizeX/edgeSize,2*SizeY/edgeSize);
Ycoord = zeros(2*SizeX/edgeSize,2*SizeY/edgeSize);
for i = 1:2*SizeX/edgeSize+1
    for j = 1:2*SizeY/edgeSize+1
        Xcoord(i,j) = -SizeX+(i-1)*edgeSize;
        Ycoord(i,j) = -SizeY+(j-1)*edgeSize;
    end
end
p = [Xcoord(:),Ycoord(:)]';

% Build t
t = zeros(3,2*2*SizeX/edgeSize*2*SizeY/edgeSize);
k = 1;
for j = 1:2*SizeY/edgeSize
    for i = 1:2*SizeX/edgeSize
        t(1,k) = i+(j-1)*(2*SizeX/edgeSize+1);
        t(2,k) = i+1+(j-1)*(2*SizeX/edgeSize+1);
        t(3,k) = i+j*(2*SizeX/edgeSize+1);
        k = k+1;
        
        t(1,k) = i+1+(j-1)*(2*SizeX/edgeSize+1);
        t(2,k) = i+1+j*(2*SizeX/edgeSize+1);
        t(3,k) = i+j*(2*SizeX/edgeSize+1);
        k = k+1;
    end
end

%% Construct C vector for CMOD+CMSD control
id1 = find(abs(R0(1:2:end)+NotchX)<TOL_g & abs(R0(2:2:end)-SizeY)<TOL_g);
id2 = find(abs(R0(1:2:end)-NotchX)<TOL_g & abs(R0(2:2:end)-SizeY)<TOL_g);
C = zeros(size(R0));
C(2*id1-1) = -1;
C(2*id2-1) = 1;
C(2*id1) = -1;
C(2*id2) = 1;

%% Stiffen locally interactions near the boundary conditions
IDPad1 = find(abs(R0(1:2:end)+SizeS2)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
IDPad2 = find(abs(R0(1:2:end)-SizeS1)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
IDPad3 = find(abs(R0(1:2:end)+SizeS1)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);
IDPad4 = find(abs(R0(1:2:end)-SizeS2)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);
IDPad = unique([IDPad1;IDPad2;IDPad3;IDPad4]);

% Stiffen locally bonds linked to Pad atoms and to their neighbors
PadNeigh = [atoms(IDPad).NeighbourList];
idPadBonds = unique([atoms(PadNeigh).BondList]);
for i = 1:length(idPadBonds)
    bonds(idPadBonds(i)).Potential = PotentialP;
end

%% Initial mesh refinement and adjustment
% Identify atoms lying at the notch's corners
id3 = find(abs(R0(1:2:end)+NotchX)<TOL_g &...
    abs(R0(2:2:end)-SizeY+NotchY)<TOL_g);
id4 = find(abs(R0(1:2:end)-NotchX)<TOL_g &...
    abs(R0(2:2:end)-SizeY+NotchY)<TOL_g);
atoms_refine = unique([id1,id2,id3,id4]);

% REFINE THE NOTCH
% Until atoms_refine are repatoms
fail = 1;
while fail
    triangles_to_refine = [];
    for i = 1:size(t,2)
        P1 = p(:,t(1,i));
        P2 = p(:,t(2,i));
        P3 = p(:,t(3,i));
        if abs(P1(1))<NotchX+TOL_g && P1(2)>SizeY-NotchY-TOL_g
            triangles_to_refine = union(triangles_to_refine,i);
        end
        if abs(P2(1))<NotchX+TOL_g && P2(2)>SizeY-NotchY-TOL_g
            triangles_to_refine = union(triangles_to_refine,i);
        end
        if abs(P3(1))<NotchX+TOL_g && P3(2)>SizeY-NotchY-TOL_g
            triangles_to_refine = union(triangles_to_refine,i);
        end
    end
    
    % Refine
    [t,p,~] = regular_refine(p,t(1:3,:),triangles_to_refine,dSize,TOL_g);
    
    % Try to find all atoms_refine in p
    fail = 0;
    for i = 1:length(atoms_refine)
        P1 = R0([2*atoms_refine(i)-1,2*atoms_refine(i)]);
        if min(abs(p(1,:)-P1(1))+abs(p(2,:)-P1(2)))>TOL_g
            fail = 1;
            break;
        end
    end
end

% Delete all triangles inside the notch
triangles_to_delete = [];
for i = 1:size(t,2)
    P1 = p(:,t(1,i));
    P2 = p(:,t(2,i));
    P3 = p(:,t(3,i));
    Cg = (P1+P2+P3)/3;
    if abs(Cg(1))<NotchX-TOL_g && Cg(2)>SizeY-NotchY-TOL_g
        triangles_to_delete = union(triangles_to_delete,i);
    end
end
t(:,triangles_to_delete) = [];
t = t(1:3,:);

% Check uniqueness of points
idPoints = unique([t(1,:),t(2,:),t(3,:)]);
tempp = NaN(2,length(idPoints));
k = 1;
for j = 1:length(idPoints)
    P1 = p(:,idPoints(j));
    if isempty(find((abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2)))<TOL_g,1))
        tempp(:,k) = P1;
        k = k+1;
    end
end
tempp = tempp(:,1:k-1);

% Renumber t
temptr = zeros(size(t));
for j = 1:size(t,2)
    
    % Find points of triangles in t within p and tempp
    P1 = p(:,t(1,j));
    P2 = p(:,t(2,j));
    P3 = p(:,t(3,j));
    ind1 = find(abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2))<TOL_g,1);
    ind2 = find(abs(tempp(1,:)-P2(1))+abs(tempp(2,:)-P2(2))<TOL_g,1);
    ind3 = find(abs(tempp(1,:)-P3(1))+abs(tempp(2,:)-P3(2))<TOL_g,1);
    if ~isempty(ind1) && ~isempty(ind2) && ~isempty(ind3)
        temptr(:,j) = [ind1;ind2;ind3];
    else
        warning('tempp for temptr renumbering failed.');
    end
end

% Assign cleared arrays for results
p = tempp;
t = temptr;

% REFINE THE PADS
% Until atoms in IDPad are repatoms
Xa = R0(2*IDPad-1);
Ya = R0(2*IDPad);
fail = 1;
while fail
    triangles_to_refine = [];
    for i = 1:size(t,2)
        P1 = p(:,t(1,i));
        P2 = p(:,t(2,i));
        P3 = p(:,t(3,i));
        if norm(cross([P3-P1;0],[P2-P1;0])) > dSize^2+TOL_g
            % Test if any atom from IDPad is within current triangle
            Bar1 = ((P2(2) - P3(2))*(Xa - P3(1)) + (P3(1) - P2(1))*...
                (Ya - P3(2))) ./ ((P2(2) - P3(2))*(P1(1) - P3(1)) +...
                (P3(1) - P2(1))*(P1(2) - P3(2)));
            Bar2 = ((P3(2) - P1(2))*(Xa - P3(1)) + (P1(1) - P3(1))*...
                (Ya - P3(2))) ./ ((P2(2) - P3(2))*(P1(1) - P3(1)) +...
                (P3(1) - P2(1))*(P1(2) - P3(2)));
            Bar3 = 1 - Bar1 - Bar2;
            % If yes, add it to triangles_to_refine
            if find(Bar1>-TOL_g & Bar2>-TOL_g & Bar3>-TOL_g,1)
                triangles_to_refine = union(triangles_to_refine,i);
            end
        end
    end
    
    % Refine
    [t,p,~] = regular_refine(p,t(1:3,:),triangles_to_refine,dSize,TOL_g);
    
    % Try to find all IDPad atoms in p
    fail = 0;
    for i = 1:length(IDPad)
        P1 = R0([2*IDPad(i)-1,2*IDPad(i)]);
        if min(abs(p(1,:)-P1(1))+abs(p(2,:)-P1(2)))>TOL_g
            fail = 1;
            break;
        end
    end
end
t(4,:) = -(1:size(t,2));

end
