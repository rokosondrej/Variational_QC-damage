function [rQC,G,H,H_DBC] = grad_hess_QC(atoms,samplingatoms,bonds,x,...
    DBCIndicesQC,DBCValuesQC,FreeIndicesQC,R0QC,Zp,Phi,maxNumThreads)

% Reconstruct r
rQC = zeros(size(R0QC));
rQC(DBCIndicesQC) = R0QC(DBCIndicesQC)+DBCValuesQC;
rQC(FreeIndicesQC) = x;
r = Phi*rQC;

% Compute data for gradient and Hessian, compute internal variables on the fly
% [f_r,I,J,S] = build_grad_hess_r_consistent_QC(atoms,samplingatoms,bonds,r,Zp,maxNumThreads);
[f_r,I,J,S] = build_grad_hess_r_secant_QC(atoms,samplingatoms,bonds,r,Zp,maxNumThreads);

% Construct the gradient
G = Phi'*f_r;
G = G(FreeIndicesQC);

% Construct the Hessian
K_r = sparse(I(:),J(:),S(:),length(r),length(r));
H = Phi'*K_r*Phi;
H_DBC = H(FreeIndicesQC,DBCIndicesQC);
H = H(FreeIndicesQC,FreeIndicesQC);

end