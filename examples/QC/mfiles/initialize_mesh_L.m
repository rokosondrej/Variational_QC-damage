function [p,t,C,bonds,idDBC] = initialize_mesh_L(atoms,bonds,SizeX1,...
    SizeY1,SizeX2,SizeY2,dSize,R0,HMax,Npad,PotentialP,TOL_g)

%% Build a regular mesh
edgeSize2 = dSize;
failure = 1;
while failure
    if mod(SizeX1,edgeSize2) == 0 && mod(SizeY1,edgeSize2) == 0 &&...
            mod(SizeX2,edgeSize2) == 0 && mod(SizeY2,edgeSize2) == 0
        if edgeSize2*2 > HMax
            failure = 0;
        else
            edgeSize1 = edgeSize2;
            edgeSize2 = edgeSize1*2;
        end
    else
        failure = 0;
    end
end
edgeSize = edgeSize1;

% Build bottom rectangle
% Build p1
Xcoord1 = zeros(SizeX1/edgeSize+1,SizeY1/edgeSize+1);
Ycoord1 = zeros(SizeX1/edgeSize+1,SizeY1/edgeSize+1);
for i = 1:SizeX1/edgeSize+1
    for j = 1:SizeY1/edgeSize+1
        Xcoord1(i,j) = 0+(i-1)*edgeSize;
        Ycoord1(i,j) = 0+(j-1)*edgeSize;
    end
end
p1 = [Xcoord1(:),Ycoord1(:)]';

% Build t1
t1 = zeros(3,2*SizeX1/edgeSize*SizeY1/edgeSize);
k = 1;
for j = 1:SizeY1/edgeSize
    for i = 1:SizeX1/edgeSize
        t1(1,k) = i+(j-1)*(SizeX1/edgeSize+1);
        t1(2,k) = i+1+(j-1)*(SizeX1/edgeSize+1);
        t1(3,k) = i+j*(SizeX1/edgeSize+1);
        k = k+1;
        
        t1(1,k) = i+1+(j-1)*(SizeX1/edgeSize+1);
        t1(2,k) = i+1+j*(SizeX1/edgeSize+1);
        t1(3,k) = i+j*(SizeX1/edgeSize+1);
        k = k+1;
    end
end

% Build upper rectangle
% Build p2
Xcoord2 = zeros(SizeX2/edgeSize+1,SizeY2/edgeSize+1);
Ycoord2 = zeros(SizeX2/edgeSize+1,SizeY2/edgeSize+1);
for i = 1:SizeX2/edgeSize+1
    for j = 1:SizeY2/edgeSize+1
        Xcoord2(i,j) = 0+(i-1)*edgeSize;
        Ycoord2(i,j) = SizeY1+(j-1)*edgeSize;
    end
end
p2 = [Xcoord2(:),Ycoord2(:)]';

% Build t2
np1 = size(p1,2);
t2 = zeros(3,2*SizeX2/edgeSize*SizeY2/edgeSize);
k = 1;
for j = 1:SizeY2/edgeSize
    for i = 1:SizeX2/edgeSize
        t2(1,k) = np1+i+(j-1)*(SizeX2/edgeSize+1);
        t2(2,k) = np1+i+1+(j-1)*(SizeX2/edgeSize+1);
        t2(3,k) = np1+i+j*(SizeX2/edgeSize+1);
        k = k+1;
        
        t2(1,k) = np1+i+1+(j-1)*(SizeX2/edgeSize+1);
        t2(2,k) = np1+i+1+j*(SizeX2/edgeSize+1);
        t2(3,k) = np1+i+j*(SizeX2/edgeSize+1);
        k = k+1;
    end
end

% Global p and t matrices
p = [p1,p2];
t = [t1,t2];

%% Remove duplicities in p and t
% Check uniqueness of points
idPoints = unique([t(1,:),t(2,:),t(3,:)]);
tempp = NaN(2,length(idPoints));
k = 1;
for j = 1:length(idPoints)
    P1 = p(:,idPoints(j));
    if isempty(find(abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2))<TOL_g,1))
        tempp(:,k) = P1;
        k = k+1;
    end
end
tempp = tempp(:,1:k-1);

% Renumber t
temptr = zeros(size(t));
for j = 1:size(t,2)
    
    % Find points of triangles in t within p and tempp
    P1 = p(:,t(1,j));
    P2 = p(:,t(2,j));
    P3 = p(:,t(3,j));
    ind1 = find(abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2))<TOL_g,1);
    ind2 = find(abs(tempp(1,:)-P2(1))+abs(tempp(2,:)-P2(2))<TOL_g,1);
    ind3 = find(abs(tempp(1,:)-P3(1))+abs(tempp(2,:)-P3(2))<TOL_g,1);
    if ~isempty(ind1) && ~isempty(ind2) && ~isempty(ind3)
        temptr(:,j) = [ind1;ind2;ind3];
    else
        warning('tempp for temptr renumbering failed.');
    end
end

% Assign cleared arrays for results
p = tempp;
t = temptr;

%% Construct C vector for CMOD control
id1 = find(abs(R0(1:2:end)-SizeX1)<TOL_g &...
    abs(R0(2:2:end)-SizeY1+dSize)<TOL_g);
id2 = find(abs(R0(1:2:end)-SizeX1-dSize)<TOL_g &...
    abs(R0(2:2:end)-SizeY1)<TOL_g);
C = zeros(size(R0));
C(2*id1) = -1;
C(2*id2) = 1;

%% Stiffen locally interactions near the Gamma_3/2 DBC condition
idPad = find(abs(R0(1:2:end)-SizeX1-(SizeX2-SizeX1)/2)<dSize*Npad+TOL_g &...
    abs(R0(2:2:end)-SizeY1)<TOL_g);
idPadBonds = unique([atoms(idPad).BondList]);
for i = 1:length(idPadBonds)
    bonds(idPadBonds(i)).Potential = PotentialP;
end

%% Initial refinement of the mesh
% Atoms that are required to be repatoms
idDBC = find(abs(R0(1:2:end)-SizeX1-(SizeX2-SizeX1)/2)<TOL_g &...
    abs(R0(2:2:end)-SizeY1)<TOL_g);
atoms_refine = unique([id1,id2,idPad,idDBC]);

% Until atoms_refine are all repatoms
Xa = R0(2*atoms_refine-1);
Ya = R0(2*atoms_refine);
fail = 1;
while fail
    triangles_to_refine = [];
    for i = 1:size(t,2)
        P1 = p(:,t(1,i));
        P2 = p(:,t(2,i));
        P3 = p(:,t(3,i));
        if norm(cross([P3-P1;0],[P2-P1;0])) > dSize^2+TOL_g
            % Test if any atom from IDPad is within current triangle
            Bar1 = ((P2(2) - P3(2))*(Xa - P3(1)) + (P3(1) - P2(1))*...
                (Ya - P3(2))) ./ ((P2(2) - P3(2))*(P1(1) - P3(1)) +...
                (P3(1) - P2(1))*(P1(2) - P3(2)));
            Bar2 = ((P3(2) - P1(2))*(Xa - P3(1)) + (P1(1) - P3(1))*...
                (Ya - P3(2))) ./ ((P2(2) - P3(2))*(P1(1) - P3(1)) +...
                (P3(1) - P2(1))*(P1(2) - P3(2)));
            Bar3 = 1 - Bar1 - Bar2;
            % If yes, add it to triangles_to_refine
            if find(Bar1>-TOL_g & Bar2>-TOL_g & Bar3>-TOL_g,1)
                triangles_to_refine = union(triangles_to_refine,i);
            end
        end
    end
    
    % Refine
    [t,p,~] = regular_refine(p,t(1:3,:),triangles_to_refine,dSize,TOL_g);
    
    % Try to find all atoms_refine in p
    fail = 0;
    for i = 1:length(atoms_refine)
        P1 = R0([2*atoms_refine(i)-1,2*atoms_refine(i)]);
        if min(abs(p(1,:)-P1(1))+abs(p(2,:)-P1(2)))>TOL_g
            fail = 1;
            break;
        end
    end
end
t(4,:) = -(1:size(t,2));

end
