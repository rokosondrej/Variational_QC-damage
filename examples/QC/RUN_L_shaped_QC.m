%% RUN_L_shaped_QC: script for example in Section 5.1 (L-shaped plate example), QC solution

%% Clear workspace
clc; % clear command line
close all; % close all figures
warning('on'); % turn on warnings
matlabrc; % restore MATLAB path, etc.
path([pwd,'/mfiles'],path); % add path to mfiles folder containing all m-files
path([pwd,'/mex'],path); % add path to mex folder containing all mex files
spparms('chmodsp') = 1;

%% Input variables

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 2;

% Geometry: construct an L-shaped region composed of bottom [0,SizeX1]x[0,SizeY1] and top [0,SizeX2]x[0,SizeY2] rectangles
dSize = 1; % lattice spacing along x- and y-axes
SizeX1 = 32*dSize; % bottom-rectanlge size along x-axis
SizeY1 = 32*dSize; % bottom-rectanlge size along y-axis
SizeX2 = 64*dSize; % top-rectanlge size along x-axis, SizeX2 > SizeX1
SizeY2 = 32*dSize; % top-rectanlge size along y-axis

% Material: Potential = [E,eps0,epsf,-]
PotentialB = [1,0.1,0.25,0]; % for bottom rectangle
PotentialU = PotentialB; % for upper rectangle
PotentialP = [1000,100,0.25,0]; % for stiff pads

% Stiff pad under DBC
Npad = 1/2;

% Tolerances
TOL_r = 1e-6; % elasticity solver realtive tolerance
TOL_i = 1e-3; % relative tolerance for the numerical integration of D'
TOL_g = 1e-10; % geometric tolerance; a positive number < TOL is treated as zero

% Summation rule: 0 - exact Beex, 1 - central summation Beex, 2 - full summation
SumRule = 1;

% Adaptive mesh algorithm
HMax = 2*max([SizeX1,SizeX2,SizeY1,SizeY2]); % initial maximal triangle edge size
theta_refine = 0.5; % 0.25, 0.5

%% Assembly initial data
fprintf('Assembling initial data...\n'),tic;
atoms = build_atoms_L([SizeX1,SizeY1,SizeX2,SizeY2,dSize,dSize],maxNumThreads);
R0 = [atoms(:).R]';
bonds = build_bonds_L(atoms,[SizeX1,SizeY1,SizeX2,SizeY2,dSize,dSize],...
    PotentialB,PotentialU,TOL_g,maxNumThreads);
[p,t,C,bonds,idDBC] = initialize_mesh_L(atoms,bonds,SizeX1,SizeY1,...
    SizeX2,SizeY2,dSize,R0,HMax,Npad,PotentialP,TOL_g);
fprintf('time consumed %g s\n',toc);

%% Solve for the evolution process of the system
fprintf('\nSolving...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., %d mesh it., CMOD = %g, %g s\n',1,0,0,0,0);
Lambda = [0,0,0];
timeIter = [0,0,0];
meshIter = [0,0,0];
Ra = [R0,zeros(2*length(atoms),2)]; % r at t-
Rb = Ra; % r at t+
Za = sparse(length(bonds),3); % z at t-
Zb = Za; % z at t+

% Indirect displacement control constant
Larc = 0.025;

i = 2;
while C'*(Rb(:,i-1)-R0) < 16
    t_start_2 = tic;
    meshConverged = 0;
    Miter = 1;
    r1 = Rb(:,i-1);
    z1 = full(Zb(:,i-1));
    lambda1 = Lambda(i-1);
    
    % MESH REFINEMENT LOOP
    while ~meshConverged
        
        if i==2 || Miter>1
            
            % ASSEMBLY/UPDATE SYSTEM DATA
            [triangles,repatoms,I,J,S] = sort_atoms_QC(p,t,atoms,TOL_g,maxNumThreads);
            Phi = accumarray([I,J],S,[2*length(atoms),...
                2*length(repatoms)],@max,[],true);
            R0QC = [atoms(repatoms).R]';
            
            % PRESCRIBE/UPDATE BOUNDARY CONDITIONS
            % Vertical BC at Gamma_3/2
            idDBCQC = find(abs(R0QC(1:2:end)-SizeX1-...
                (SizeX2-SizeX1)/2)<TOL_g & abs(R0QC(2:2:end)-SizeY1)<TOL_g);
            
            % Fix the bottom edge Gamma_1
            IDGamma_1QC = find(R0QC(2:2:end)<TOL_g);
            
            % Assembly CMOD control vector C
            id1QC = find(abs(R0QC(1:2:end)-SizeX1)<TOL_g &...
                abs(R0QC(2:2:end)-SizeY1+dSize)<TOL_g);
            id2QC = find(abs(R0QC(1:2:end)-SizeX1-dSize)<TOL_g &...
                abs(R0QC(2:2:end)-SizeY1)<TOL_g);
            CQC = zeros(size(R0QC));
            CQC(2*id1QC) = -1;
            CQC(2*id2QC) = 1;
            
            % Extract code numbers
            DBCIndicesQC = [2*IDGamma_1QC-1;2*IDGamma_1QC]; % code numbers for constrained repatoms
            DBCValuesQC = 0*DBCIndicesQC; % prescribed displacements for constrained repatoms
            FreeIndicesQC = setdiff(1:2*length(repatoms),DBCIndicesQC)'; % all free repatoms
            FextQC = zeros(size(R0QC));
            FextQC(2*idDBCQC) = 1;
            
            % Determine sampling atoms and bonds
            samplingatoms = sort_sampling_atoms_QC(atoms,triangles,...
                SumRule,[SizeX1,SizeY1,SizeX2,SizeY2],0,TOL_g);
            samplingatomsID = [samplingatoms(:).ID]';
            samplingbondsID = unique([atoms(samplingatomsID).BondList])';
            
            % Plot atoms, repatoms, sampling atoms, and triangulation
            figure(1); clf, hold all, axis equal, xlabel('x'), ylabel('y');
            pdemesh(p,[],t);
            plot(R0(1:2:end),R0(2:2:end),'.k','MarkerSize',1);
            plot(R0(2*samplingatomsID-1),...
                R0(2*samplingatomsID),'.m','MarkerSize',5);
            plot(R0QC(1:2:end),R0QC(2:2:end),'.g','MarkerSize',5);
            drawnow;
        end
        
        % MINIMIZE THE REDUCED ENERGY WITH RESPECT TO r
        [r2,Niter,lambda2] = minimize_r_QC(atoms,samplingatoms,bonds,...
            r1,Rb(:,i-1),DBCIndicesQC,DBCValuesQC,FextQC,FreeIndicesQC,...
            R0QC,z1,CQC,Phi,TOL_r,Larc,lambda1,maxNumThreads);
        
        % MINIMIZE WITH RESPECT TO z
        z2 = update_damage_QC(atoms,bonds,1:length(bonds),r2,...
            full(Zb(:,i-1)),maxNumThreads);
        
        % Store outputs at t-
        if Miter == 1
            Ra(:,i) = r2;
            Za(:,i) = z2;
            SAMPLINGATOMSa{i} = samplingatoms;
            PHIa{i} = Phi;
        end
        
        % Update iteration data
        r1 = r2;
        z1 = z2;
        lambda1 = lambda2;
        timeIter(i) = timeIter(i)+Niter;
        
        % Assembly mesh indicator for the next mesh iteration
        refine_triangles = error_indicator_QC(p,t(1:3,:),atoms,bonds,...
            samplingbondsID,r2,z2,theta_refine,TOL_g,maxNumThreads);
        [t,p,meshConverged] = regular_refine(p,t,refine_triangles,...
            dSize,TOL_g);
        
        % Proceed to next mesh iteration
        Miter = Miter+1;
    end
    
    % Store outputs at t+
    if i==2
        SAMPLINGATOMSa{1} = samplingatoms;
        SAMPLINGATOMSb{1} = samplingatoms;
        PHIa{1} = Phi;
        PHIb{1} = Phi;
        REPATOMSb{1} = repatoms;
        SAMPLINGBONDSIDb{1} = samplingbondsID;
        TRIANGLESb{1} = triangles;
    end
    Rb(:,i) = r2;
    Zb(:,i) = z2;
    Lambda(i) = lambda2;
    SAMPLINGATOMSb{i} = samplingatoms;
    PHIb{i} = Phi;
    REPATOMSb{i} = repatoms;
    SAMPLINGBONDSIDb{i} = samplingbondsID;
    TRIANGLESb{i} = triangles;
    
    % Print iteration message
    fprintf('%d step, %d Nwtn it., %d Mesh it., CMOD = %g, %g s\n',i,...
        timeIter(i),Miter-2,C'*(Rb(:,i)-R0),toc(t_start_2));
    meshIter(i) = Miter-2;
    timeIter(i+1) = 0;
    
    % Proceed to the next time step
    i = i+1;
end

% Print solution statistics
fprintf('\nE[Miter] %g\n',mean(meshIter));
fprintf('Max[Miter] %g\n',max(meshIter));
fprintf('E[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n',toc(t_start_1));

%% Draw results
% Plot atoms in deformed configuration
scale = 1;
param = C'*Rb-C'*R0;
step = length(param);
figure(2),clf,hold all,axis equal,title(['Deformed structure, scale = ',...
    num2str(scale)]),xlabel('x'),ylabel('y');
plot(R0(1:2:end)+scale*(Rb(1:2:end,step)-R0(1:2:end)),...
    R0(2:2:end)+scale*(Rb(2:2:end,step)-R0(2:2:end)),'.k','MarkerSize',5);

% Compute reactions
REACT = zeros(size(Rb));
for i = 2:length(param)
    sampatomsb = SAMPLINGATOMSb{i};
    REACT(:,i) = build_grad_r_QC(atoms,sampatomsb,bonds,Rb(:,i),...
        full(Zb(:,i)),maxNumThreads);
end

% Compute energy evolutions
Ena = zeros(1,length(param)); % elastic energy at t-
Enb = zeros(1,length(param)); % elastic energy at t+
DissDist = zeros(1,length(param)); % dissipation distances
Dij = zeros(1,length(param)); % dissipation distances, physical
Dab = zeros(1,length(param)); % dissipation distances, artificial
We = zeros(1,length(param)); % work increments done by external forces (reactions), reconstructed
Wij = zeros(1,length(param)); % work increments done by external forces (reactions), physical
Wab = zeros(1,length(param)); % work increments done by external forces (reactions), artificial
F = zeros(1,size(Rb,2)); % reaction force
for i = 2:length(param)
    sampatomsa = SAMPLINGATOMSa{i};
    sampatomsb = SAMPLINGATOMSb{i};
    Phia = PHIa{i};
    Phib = PHIb{i};
    
    Ena(i) = build_en_QC(atoms,sampatomsa,bonds,full(Za(:,i)),Ra(:,i),maxNumThreads);
    Enb(i) = build_en_QC(atoms,sampatomsb,bonds,full(Zb(:,i)),Rb(:,i),maxNumThreads);
    
    Dij(i) = build_diss_QC(atoms,sampatomsa,bonds,Ra(:,i),Rb(:,i-1),TOL_i,maxNumThreads);
    Dab(i) = build_diss_QC(atoms,sampatomsb,bonds,Rb(:,i),Ra(:,i),TOL_i,maxNumThreads);
    
    REACTQCI = Phia'*build_grad_r_QC(atoms,sampatomsa,bonds,Rb(:,i-1),...
        full(Zb(:,i-1)),maxNumThreads);
    REACTQCJ = Phia'*build_grad_r_QC(atoms,sampatomsa,bonds,Ra(:,i),...
        full(Za(:,i)),maxNumThreads);
    RQCI = (Phia'*Phia)\(Phia'*Rb(:,i-1));
    RQCJ = (Phia'*Phia)\(Phia'*Ra(:,i));
    Wij(i) = 0.5*(REACTQCI+REACTQCJ)'*(RQCJ-RQCI);
    
    REACTQCa = Phib'*build_grad_r_QC(atoms,sampatomsb,bonds,Ra(:,i),...
        full(Za(:,i)),maxNumThreads);
    REACTQCb = Phib'*build_grad_r_QC(atoms,sampatomsb,bonds,Rb(:,i),...
        full(Zb(:,i)),maxNumThreads);
    RQCa = (Phib'*Phib)\(Phib'*Ra(:,i));
    RQCb = (Phib'*Phib)\(Phib'*Rb(:,i));
    Wab(i) = 0.5*(REACTQCa+REACTQCb)'*(RQCb-RQCa);
    
    We(i) = Wij(i)+Wab(i);
    DissDist(i) = Dij(i)+Dab(i);
    
    repatoms = REPATOMSb{i};
    id = find(repatoms==idDBC);
    F(i) = REACTQCb(2*id);
end
VarD = cumsum(DissDist); % reconstructed dissipated energy
Wext = cumsum(We); % reconstructed work done by external forces

% Plot energy evolution paths
figure(3);clf,hold all,box on,xlabel('time step [-]'),...
    ylabel('Energy [kJ]');
plot(param,Enb+VarD,'--k','linewidth',1);
plot(param,Enb,'k');
plot(param,VarD,'-.k');
plot(param,Wext,':k');
legend('E+Var_D','V','Var_D','W_{ext}','location','northwest');

% Plot integrated energy increments
figure(4);clf,hold all,box on,xlabel('time step [-]'),...
    ylabel('Energy [kJ]');
plot(param,cumsum(Ena-Enb),'k','linewidth',1);
plot(param,cumsum(Dab),':k');
plot(param,cumsum(Wab),'--k');
plot(param,cumsum(Ena-Enb-Dab+Wab),'--k','linewidth',1);
plot(param,cumsum(Dij),'-.k');
plot(param,cumsum(Wij),'k');
legend('-E_A','Var_{D,A}','W_{ext,A}','W_{ext,A}-E_A-Var_{D,A}',...
    'Var_{D,P}','W_{ext,P}');

% Plot reaction force
figure(5);clf,hold all,box on;
xlabel('\lambda(t)'),ylabel('F');
plot(param,Lambda,'k');

%% Export data to Paraview
U = zeros(size(R0));
for istep = 1:length(param)
    U(:,istep) = Rb(:,istep)-R0;
end
step = 5;
ParaView_export_2d_QC('qc_L',atoms,bonds,TRIANGLESb(1:step:end),...
    SAMPLINGATOMSb(1:step:end),R0,U(:,1:step:end),Zb(:,1:step:end),...
    param(1:step:end),maxNumThreads);
