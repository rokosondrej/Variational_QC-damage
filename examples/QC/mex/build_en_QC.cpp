// build_en_QC: computes the approximate elastic part of the incremental
// energy of the system

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for six input argument
	if (nrhs != 6)
		mexErrMsgTxt("Six input arguments required!\n");

	// Get the data
	double *prOmega = mxGetPr(prhs[3]);
	double *prx = mxGetPr(prhs[4]);
	int maxNumThreads = (int)mxGetScalar(prhs[5]);

	// Compute energy
	nlhs = 1;
	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *prEn = mxGetPr(plhs[0]);
	int i;
	double En = 0;
	int NsAtoms = (int)mxGetNumberOfElements(prhs[1]);
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_NeighbourList = mxGetFieldNumber(prhs[0], "NeighbourList");
	int name_BondList = mxGetFieldNumber(prhs[0], "BondList");
	int name_Potential = mxGetFieldNumber(prhs[2], "Potential");
	int name_ID = mxGetFieldNumber(prhs[1], "ID");
	int name_w = mxGetFieldNumber(prhs[1], "w");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _j, _alpha, _beta, _bond;
		double _R, _r, _omega, _w, _EnInt;
		double *_Ra, *_Rb, *_NeighbourList, *_BondList, *_prID,
			*_Potential, *_prw;
		double _ra[2], _rab[2], _Rab[2];
		mxArray *_pNeighbourList;

#pragma omp for reduction(+ \
						  : En)
		for (i = 0; i < NsAtoms; i++)
		{ // loop over all sampling atoms
			_prw = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_w));
			_prID = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_ID));
			_w = _prw[0];
			_alpha = (int)_prID[0]; // atom alpha ID, MATLAB indexing

			// Get data for sampling atom alpha
			_Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
			_pNeighbourList = mxGetFieldByNumber(prhs[0], _alpha - 1, name_NeighbourList);
			_NeighbourList = mxGetPr(_pNeighbourList);
			_BondList = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_BondList));
			_ra[0] = prx[2 * (_alpha - 1)];
			_ra[1] = prx[2 * (_alpha - 1) + 1];
			for (_j = 0; _j < (int)mxGetN(_pNeighbourList); _j++)
			{									 // loop over all nearest neighbours of sampling atom alpha
				_beta = (int)_NeighbourList[_j]; // atom beta ID, MATLAB indexing

				// Get data for atom beta
				_Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
				_Rab[0] = _Rb[0] - _Ra[0];
				_Rab[1] = _Rb[1] - _Ra[1];
				_rab[0] = prx[2 * (_beta - 1)] - _ra[0];
				_rab[1] = prx[2 * (_beta - 1) + 1] - _ra[1];
				_R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length
				_r = sqrt(_rab[0] * _rab[0] + _rab[1] * _rab[1]); // deformed bond length

				// Get data for the bond between atoms alpha and beta
				_bond = (int)_BondList[_j];
				_Potential = mxGetPr(mxGetFieldByNumber(prhs[2], _bond - 1, name_Potential));
				_omega = prOmega[_bond - 1]; // current damage

				// Compute internal part of the energy
				if (_r > _R)
					_EnInt = (1 - _omega) * 0.25 * (_Potential[0] / _R) * pow(_r - _R, 2);
				else
					_EnInt = 0.25 * (_Potential[0] / _R) * pow(_r - _R, 2);

				// Add to the overall energy
				En += _w * _EnInt;
			}
		}
	}

	// Send the data back to MATLAB
	prEn[0] = En;
}
