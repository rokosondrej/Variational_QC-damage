// build_atoms_4: builds a database of all atoms for the four point
// bending test.
// Implementation below exploits specific numbering of atoms from -SizeX to
// +SizeX and from -SizeY to +SizeY.

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/******************** Structure declarations ******************/
struct ATOM
{
	double R[2];		  // coordinates of an atom in undeformed configuration, R = [x,y]
	int NeighbourList[9]; // length followed by a list of all the nearest neighbours, max length 9
};

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for three input arguments
	if (nrhs != 3)
		mexErrMsgTxt("Three input argument required!\n");

	// Copy the input variables
	double *prrhs;
	prrhs = mxGetPr(prhs[0]);
	int Nrhs = (int)mxGetN(prhs[0]);
	int maxNumThreads = (int)mxGetScalar(prhs[2]);

	// Test for the correct length of the input vector
	if (Nrhs != 6)
	{
		printf("\nInput vector length must equal to 6.\n");
		return;
	}
	double SizeX = prrhs[0];
	double SizeY = prrhs[1];
	double NotchX = prrhs[2];
	double NotchY = prrhs[3];
	double dSizeX = prrhs[4];
	double dSizeY = prrhs[5];
	double *prTOL = mxGetPr(prhs[1]);
	double TOL = prTOL[0]; // a positive distance < TOL is treated as zero

	// Build database of all atoms
	int i, j;
	int NxAtoms = (int)floor(2 * SizeX / dSizeX) + 1;	 // number of atoms in x-direction
	int NyAtoms = (int)floor(2 * SizeY / dSizeY) + 1;	 // number of atoms in y-direction
	int NxRAtoms = (int)(2 * NotchX / dSizeX - 1);		  // number of atoms in x-direction along the notch
	int NyRatoms = (int)(NotchY / dSizeY);				  // number of atoms in y-direction along the notch
	int NAtoms = NxAtoms * NyAtoms - NxRAtoms * NyRatoms; // number of atoms
	ATOM *atoms = new ATOM[NAtoms];						  // initialize the database of all atoms

	// Test for succesfull initialization
	if (atoms == NULL)
	{
		printf("\nNot enough memory for atoms database!\n");
		return;
	}

	// Fill in undeformed positions
	double R[2];
	int count = 0;
	for (j = 0; j < NyAtoms; j++)
	{
		for (i = 0; i < NxAtoms; i++)
		{
			R[0] = -SizeX + i * dSizeX;
			R[1] = -SizeY + j * dSizeY;
			if ((fabs(R[0]) < NotchX - TOL) &&
				(R[1] > SizeY - NotchY + TOL))
			{
				// The notch subdomain
			}
			else
			{
				atoms[count].R[0] = R[0]; // x-coordinate
				atoms[count].R[1] = R[1]; // y-coordinate
				count++;
			}
		}
	}

// Build NeighbourList for all atoms
// Bottom rectangle
#pragma omp parallel num_threads(maxNumThreads)
	{
#pragma omp for
		for (i = 0; i < NxAtoms * (NyAtoms - NyRatoms - 1); i++)
		{
			if (atoms[i].R[0] == -SizeX)
			{
				if (atoms[i].R[1] == -SizeY)
				{												 // [-SizeX,-SizeY] corner
					atoms[i].NeighbourList[0] = 3;				 // 3 neighbours
					atoms[i].NeighbourList[1] = i + 2;			 // right, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 1 + NxAtoms; // up
					atoms[i].NeighbourList[3] = i + 2 + NxAtoms; // up right
				}
				if ((atoms[i].R[1] > -SizeY) && (atoms[i].R[1] < SizeY))
				{												 // [-SizeX,Y] side
					atoms[i].NeighbourList[0] = 5;				 // 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms; // down, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms; // down right
					atoms[i].NeighbourList[3] = i + 2;			 // right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms; // up right
				}
			}
			if (atoms[i].R[0] == SizeX)
			{
				if (atoms[i].R[1] == -SizeY)
				{												 // [SizeX,-SizeY] corner
					atoms[i].NeighbourList[0] = 3;				 // 3 neighbours
					atoms[i].NeighbourList[1] = i;				 // left, MATLAB indexing
					atoms[i].NeighbourList[2] = i + NxAtoms;	 // up left
					atoms[i].NeighbourList[3] = i + 1 + NxAtoms; // up
				}
				if ((atoms[i].R[1] > -SizeY) && (atoms[i].R[1] < SizeY))
				{												 // [SizeX,Y] side
					atoms[i].NeighbourList[0] = 5;				 // 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms;	 // down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms; // down
					atoms[i].NeighbourList[3] = i;				 // left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms;	 // up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms; // up
				}
			}
			if ((atoms[i].R[1] == -SizeY) && (atoms[i].R[0] > -SizeX) &&
				(atoms[i].R[0] < SizeX))
			{												 // [X,-SizeY] side
				atoms[i].NeighbourList[0] = 5;				 // 5 neighbours
				atoms[i].NeighbourList[1] = i;				 // left, MATLAB indexing
				atoms[i].NeighbourList[2] = i + 2;			 // right
				atoms[i].NeighbourList[3] = i + NxAtoms;	 // up left
				atoms[i].NeighbourList[4] = i + 1 + NxAtoms; // up
				atoms[i].NeighbourList[5] = i + 2 + NxAtoms; // up right
			}
			if ((atoms[i].R[0] > -SizeX) && (atoms[i].R[0] < SizeX) &&
				(atoms[i].R[1] > -SizeY) && (atoms[i].R[1] < SizeY))
			{												 // all internal atoms
				atoms[i].NeighbourList[0] = 8;				 // 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms; // down right
				atoms[i].NeighbourList[4] = i;				 // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			 // right
				atoms[i].NeighbourList[6] = i + NxAtoms;	 // up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms; // up right
			}
		}

		// Glue the three rectangles together
#pragma omp for
		for (i = NxAtoms * (NyAtoms - NyRatoms - 1);
			 i < NxAtoms * (NyAtoms - NyRatoms); i++)
		{
			if (atoms[i].R[0] == -SizeX)
			{
				atoms[i].NeighbourList[0] = 5;				 // 5 neighbours
				atoms[i].NeighbourList[1] = i + 1 - NxAtoms; // down
				atoms[i].NeighbourList[2] = i + 2 - NxAtoms; // down right
				atoms[i].NeighbourList[3] = i + 2;			 // right
				atoms[i].NeighbourList[4] = i + 1 + NxAtoms; // up
				atoms[i].NeighbourList[5] = i + 2 + NxAtoms; // up right
			}
			if ((atoms[i].R[0] > -SizeX) && (atoms[i].R[0] < -NotchX))
			{
				atoms[i].NeighbourList[0] = 8;				 // 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms; // down right
				atoms[i].NeighbourList[4] = i;				 // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			 // right
				atoms[i].NeighbourList[6] = i + NxAtoms;	 // up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms; // up right
			}
			if (atoms[i].R[0] == -NotchX)
			{
				atoms[i].NeighbourList[0] = 7;				 // 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms; // down right
				atoms[i].NeighbourList[4] = i;				 // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			 // right
				atoms[i].NeighbourList[6] = i + NxAtoms;	 // up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms; // up
			}
			if ((atoms[i].R[0] > -NotchX) && (atoms[i].R[0] < NotchX))
			{
				atoms[i].NeighbourList[0] = 5;				 // 5 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms; // down right
				atoms[i].NeighbourList[4] = i;				 // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			 // right
			}
			if (atoms[i].R[0] == NotchX)
			{
				atoms[i].NeighbourList[0] = 7;							// 7 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;				// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms;			// down right
				atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;						// right
				atoms[i].NeighbourList[6] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[7] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if ((atoms[i].R[0] > NotchX) && (atoms[i].R[0] < SizeX))
			{
				atoms[i].NeighbourList[0] = 8;							// 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;				// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms;			// down right
				atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;						// right
				atoms[i].NeighbourList[6] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if (atoms[i].R[0] == SizeX)
			{
				atoms[i].NeighbourList[0] = 5;							// 5 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;				// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
			}
		}

		// First layer
#pragma omp for
		for (i = NxAtoms * (NyAtoms - NyRatoms);
			 i < NxAtoms * (NyAtoms - NyRatoms) + NxAtoms - NxRAtoms; i++)
		{
			if (atoms[i].R[0] == -SizeX)
			{
				atoms[i].NeighbourList[0] = 5;							// 5 neighbours
				atoms[i].NeighbourList[1] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[2] = i + 2 - NxAtoms;			// down right
				atoms[i].NeighbourList[3] = i + 2;						// right
				atoms[i].NeighbourList[4] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[5] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if ((atoms[i].R[0] > -SizeX) && (atoms[i].R[0] < -NotchX))
			{
				atoms[i].NeighbourList[0] = 8;							// 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;				// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms;			// down right
				atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;						// right
				atoms[i].NeighbourList[6] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if (atoms[i].R[0] == -NotchX)
			{
				atoms[i].NeighbourList[0] = 5;							// 5 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms;				// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms;			// down
				atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
			}
			if (atoms[i].R[0] == NotchX)
			{
				atoms[i].NeighbourList[0] = 5;							// 5 neighbours
				atoms[i].NeighbourList[1] = i + 1 - NxAtoms + NxRAtoms; // down
				atoms[i].NeighbourList[2] = i + 2 - NxAtoms + NxRAtoms; // down right
				atoms[i].NeighbourList[3] = i + 2;						// right
				atoms[i].NeighbourList[4] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[5] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if ((atoms[i].R[0] > NotchX) && (atoms[i].R[0] < SizeX))
			{
				atoms[i].NeighbourList[0] = 8;							// 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms + NxRAtoms; // down right
				atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;						// right
				atoms[i].NeighbourList[6] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms - NxRAtoms; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms - NxRAtoms; // up right
			}
			if (atoms[i].R[0] == SizeX)
			{
				atoms[i].NeighbourList[0] = 5;							// 5 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
				atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
				atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
				atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
			}
		}

		// The remaining atoms
#pragma omp for
		for (i = NxAtoms * (NyAtoms - NyRatoms) + NxAtoms - NxRAtoms;
			 i < NAtoms; i++)
		{
			if (atoms[i].R[0] == -SizeX)
			{
				if (atoms[i].R[1] < SizeY)
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[3] = i + 2;						// right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms - NxRAtoms; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms - NxRAtoms; // up right
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 3;							// 3 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[3] = i + 2;						// right
				}
			}
			if ((atoms[i].R[0] > -SizeX) && (atoms[i].R[0] < -NotchX))
			{
				if (atoms[i].R[1] < SizeY)
				{
					atoms[i].NeighbourList[0] = 8;							// 8 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[5] = i + 2;						// right
					atoms[i].NeighbourList[6] = i + NxAtoms - NxRAtoms;		// up left
					atoms[i].NeighbourList[7] = i + 1 + NxAtoms - NxRAtoms; // up
					atoms[i].NeighbourList[8] = i + 2 + NxAtoms - NxRAtoms; // up right
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[5] = i + 2;						// right
				}
			}
			if (atoms[i].R[0] == -NotchX)
			{
				if ((atoms[i].R[1] < SizeY))
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 3;							// 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
				}
			}
			if (atoms[i].R[0] == NotchX)
			{
				if ((atoms[i].R[1] < SizeY))
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[3] = i + 2;						// right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms - NxRAtoms; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms - NxRAtoms; // up right
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 3;							// 3 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[3] = i + 2;						// right
				}
			}
			if ((atoms[i].R[0] > NotchX) && (atoms[i].R[0] < SizeX))
			{
				if (atoms[i].R[1] < SizeY)
				{
					atoms[i].NeighbourList[0] = 8;							// 8 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[5] = i + 2;						// right
					atoms[i].NeighbourList[6] = i + NxAtoms - NxRAtoms;		// up left
					atoms[i].NeighbourList[7] = i + 1 + NxAtoms - NxRAtoms; // up
					atoms[i].NeighbourList[8] = i + 2 + NxAtoms - NxRAtoms; // up right
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i + 2 - NxAtoms + NxRAtoms; // down right
					atoms[i].NeighbourList[4] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[5] = i + 2;						// right
				}
			}
			if (atoms[i].R[0] == SizeX)
			{
				if (atoms[i].R[1] < SizeY)
				{
					atoms[i].NeighbourList[0] = 5;							// 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms - NxRAtoms;		// up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms - NxRAtoms; // up
				}
				else if (atoms[i].R[1] == SizeY)
				{
					atoms[i].NeighbourList[0] = 3;							// 3 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms + NxRAtoms;		// down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms + NxRAtoms; // down
					atoms[i].NeighbourList[3] = i;							// left, MATLAB indexing
				}
			}
		}
	}

	// Send out the data back to MATLAB
	nlhs = 1;
	mwSize dims[] = {1, (mwSize)NAtoms};
	const char *field_names[] = {"R", "NeighbourList", "BondList"};
	plhs[0] = mxCreateStructArray(2, dims, 3, field_names);
	double *prR, *prNeighbourList;
	mxArray *array_R, *array_NeighbourList, *array_BondList;
	int name_R = mxGetFieldNumber(plhs[0], "R");
	int name_NeighbourList = mxGetFieldNumber(plhs[0], "NeighbourList");
	int name_BondList = mxGetFieldNumber(plhs[0], "BondList");

	// Populate the output with computed data (mx functions are not thread-safe)
	for (i = 0; i < NAtoms; i++)
	{

		// Create arrays
		array_R = mxCreateDoubleMatrix(1, 2, mxREAL);
		array_NeighbourList = mxCreateDoubleMatrix(1, atoms[i].NeighbourList[0], mxREAL);
		array_BondList = mxCreateDoubleMatrix(1, atoms[i].NeighbourList[0], mxREAL);

		// Get data arrays
		prR = mxGetPr(array_R);
		prNeighbourList = mxGetPr(array_NeighbourList);

		// Populate data arrays
		for (j = 0; j < 2; j++)
			prR[j] = atoms[i].R[j];
		for (j = 0; j < atoms[i].NeighbourList[0]; j++)
			prNeighbourList[j] = atoms[i].NeighbourList[j + 1];

		// Assign the arrays to the output structure
		mxSetFieldByNumber(plhs[0], i, name_R, array_R);
		mxSetFieldByNumber(plhs[0], i, name_NeighbourList, array_NeighbourList);
		mxSetFieldByNumber(plhs[0], i, name_BondList, array_BondList);
	}

	// Delete atoms database
	delete[] atoms;
}
