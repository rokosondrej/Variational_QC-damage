// build_atoms: builds a database of all atoms.
// Implementation below exploits specific numbering of atoms from [0,0] to
// [SizeX2,SizeY1+SizeY2].

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/******************** Structure declarations ******************/
struct ATOM
{
	double R[2];		  // coordinates of an atom in undeformed configuration, R = [x,y]
	int NeighbourList[9]; // length followed by a list of all the nearest neighbours, max length 9
};

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for two input argument
	if (nrhs != 2)
		mexErrMsgTxt("Two input argument required!\n");

	// Copy the input variables
	double *prRect = mxGetPr(prhs[0]);
	int Nrhs = (int)mxGetN(prhs[0]);
	int maxNumThreads = (int)mxGetScalar(prhs[1]);

	// Test for the correct length of the input vector
	if (Nrhs != 6)
		mexErrMsgTxt("Input vector length must equal to 6.\n");
	double SizeX1 = prRect[0];
	double SizeY1 = prRect[1];
	double SizeX2 = prRect[2];
	double SizeY2 = prRect[3];
	double dSizeX = prRect[4];
	double dSizeY = prRect[5];

	// Build database of all atoms
	int i, j;
	int NxAtoms1 = (int)floor(SizeX1 / dSizeX) + 1; // number of atoms in x-direction, bottom rectangle
	int NyAtoms1 = (int)floor(SizeY1 / dSizeY);		// number of atoms in y-direction, bottom rectangle
	int NxAtoms2 = (int)floor(SizeX2 / dSizeX) + 1; // number of atoms in x-direction, upper rectangle
	int NyAtoms2 = (int)floor(SizeY2 / dSizeY) + 1; // number of atoms in y-direction, upper rectangle
	int NAtoms1 = NxAtoms1 * NyAtoms1;				// number of atoms in bottom rectangle
	int NAtoms2 = NxAtoms2 * NyAtoms2;				// number of atoms in upper rectangle
	int NAtoms = NAtoms1 + NAtoms2;					// number of atoms
	ATOM *atoms = new ATOM[NAtoms];					// init the database of all atoms

	// Test for succesfull initialization
	if (atoms == NULL)
	{
		printf("\nNot enough memory for atoms database!\n");
		return;
	}

	// Fill in the undeformed positions
	int count = 0;
	for (j = 0; j < NyAtoms1; j++)
	{
		for (i = 0; i < NxAtoms1; i++)
		{
			atoms[count].R[0] = 0 + i * dSizeX; // x-coordinate
			atoms[count].R[1] = 0 + j * dSizeY; // y-coordinate
			count++;
		}
	}
	for (j = 0; j < NyAtoms2; j++)
	{
		for (i = 0; i < NxAtoms2; i++)
		{
			atoms[count].R[0] = 0 + i * dSizeX;		 // x-coordinate
			atoms[count].R[1] = SizeY1 + j * dSizeY; // y-coordinate
			count++;
		}
	}

	// Build NeighbourList for all atoms
#pragma omp parallel num_threads(maxNumThreads)
	{
		// Bottom rectangle
#pragma omp for
		for (i = 0; i < NAtoms1; i++)
		{
			if (atoms[i].R[0] == 0)
			{
				if (atoms[i].R[1] == 0)
				{												  // [0,0] corner
					atoms[i].NeighbourList[0] = 3;				  // 3 neighbours
					atoms[i].NeighbourList[1] = i + 2;			  // right, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 1 + NxAtoms1; // up
					atoms[i].NeighbourList[3] = i + 2 + NxAtoms1; // up right
				}
				if (atoms[i].R[1] > 0)
				{												  // [0,Y] side
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms1; // down, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms1; // down right
					atoms[i].NeighbourList[3] = i + 2;			  // right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms1; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms1; // up right
				}
			}
			if (atoms[i].R[0] == SizeX1)
			{
				if (atoms[i].R[1] == 0)
				{												  // [SizeX1,0] corner
					atoms[i].NeighbourList[0] = 3;				  // 3 neighbours
					atoms[i].NeighbourList[1] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[2] = i + NxAtoms1;	 // up left
					atoms[i].NeighbourList[3] = i + 1 + NxAtoms1; // up
				}
				if (atoms[i].R[1] > 0)
				{												  // [SizeX1,Y] corner
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms1;	 // down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms1; // down
					atoms[i].NeighbourList[3] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms1;	 // up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms1; // up
				}
			}
			if ((atoms[i].R[1] == 0) && (atoms[i].R[0] > 0) &&
				(atoms[i].R[0] < SizeX1))
			{												  // [X,0] side
				atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
				atoms[i].NeighbourList[1] = i;				  // left, MATLAB indexing
				atoms[i].NeighbourList[2] = i + 2;			  // right
				atoms[i].NeighbourList[3] = i + NxAtoms1;	 // up left
				atoms[i].NeighbourList[4] = i + 1 + NxAtoms1; // up
				atoms[i].NeighbourList[5] = i + 2 + NxAtoms1; // up right
			}
			if ((atoms[i].R[0] > 0) && (atoms[i].R[0] < SizeX1) &&
				(atoms[i].R[1] > 0) && (atoms[i].R[1] < SizeY1))
			{												  // all internal atoms
				atoms[i].NeighbourList[0] = 8;				  // 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms1;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms1; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms1; // down right
				atoms[i].NeighbourList[4] = i;				  // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			  // right
				atoms[i].NeighbourList[6] = i + NxAtoms1;	 // up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms1; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms1; // up right
			}
		}

		// Upper rectangle
#pragma omp for
		for (i = NAtoms1; i < NAtoms1 + NAtoms2; i++)
		{
			if (atoms[i].R[0] == 0)
			{
				if (atoms[i].R[1] == SizeY1)
				{												  // [0,SizeY1] corner
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms1; // down, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms1; // down right
					atoms[i].NeighbourList[3] = i + 2;			  // right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms2; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms2; // up right
				}
				if (atoms[i].R[1] == SizeY1 + SizeY2)
				{												  // [0,SizeY1+SizeY2] corner
					atoms[i].NeighbourList[0] = 3;				  // 3 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms2; // down, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms2; // down right
					atoms[i].NeighbourList[3] = i + 2;			  // right
				}
				if ((atoms[i].R[1] > SizeY1) &&
					(atoms[i].R[1] < SizeY1 + SizeY2))
				{												  // [0,Y] side
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i + 1 - NxAtoms2; // down, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2 - NxAtoms2; // down right
					atoms[i].NeighbourList[3] = i + 2;			  // right
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms2; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms2; // up right
				}
			}
			if (atoms[i].R[0] == SizeX2)
			{
				if (atoms[i].R[1] == SizeY1)
				{												  // [SizeX2,SizeY1] corner
					atoms[i].NeighbourList[0] = 3;				  // 3 neighbours
					atoms[i].NeighbourList[1] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[2] = i + NxAtoms2;	 // up left
					atoms[i].NeighbourList[3] = i + 1 + NxAtoms2; // up
				}
				if (atoms[i].R[1] == SizeY1 + SizeY2)
				{												  // [SizeX2,SizeY1+SizeY2] corner
					atoms[i].NeighbourList[0] = 3;				  // 3 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms2;	 // down left, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms2; // down
					atoms[i].NeighbourList[3] = i;				  // left
				}
				if ((atoms[i].R[1] > SizeY1) &&
					(atoms[i].R[1] < SizeY1 + SizeY2))
				{												  // [SizeX2,Y] side
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms2;	 // down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms2; // down
					atoms[i].NeighbourList[3] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + NxAtoms2;	 // up left
					atoms[i].NeighbourList[5] = i + 1 + NxAtoms2; // up
				}
			}
			if (atoms[i].R[1] == SizeY1)
			{
				if ((atoms[i].R[0] > 0) && (atoms[i].R[0] < SizeX1))
				{												  // [X in SizeX1,SizeY] side
					atoms[i].NeighbourList[0] = 8;				  // 8 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms1;	 // down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms1; // down
					atoms[i].NeighbourList[3] = i + 2 - NxAtoms1; // down right
					atoms[i].NeighbourList[4] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[5] = i + 2;			  // right
					atoms[i].NeighbourList[6] = i + NxAtoms2;	 // up left
					atoms[i].NeighbourList[7] = i + 1 + NxAtoms2; // up
					atoms[i].NeighbourList[8] = i + 2 + NxAtoms2; // up right
				}
				if (atoms[i].R[0] == SizeX1)
				{												  // [SizeX1,SizeY1] corner
					atoms[i].NeighbourList[0] = 7;				  // 7 neighbours
					atoms[i].NeighbourList[1] = i - NxAtoms1;	 // down left
					atoms[i].NeighbourList[2] = i + 1 - NxAtoms1; // down
					atoms[i].NeighbourList[3] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[4] = i + 2;			  // right
					atoms[i].NeighbourList[5] = i + NxAtoms2;	 // up left
					atoms[i].NeighbourList[6] = i + 1 + NxAtoms2; // up
					atoms[i].NeighbourList[7] = i + 2 + NxAtoms2; // up right
				}
				if ((atoms[i].R[0] > SizeX1) && (atoms[i].R[0] < SizeX2))
				{												  // [X in SizeX2\SizeX1,SizeY1] side
					atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
					atoms[i].NeighbourList[1] = i;				  // left, MATLAB indexing
					atoms[i].NeighbourList[2] = i + 2;			  // right
					atoms[i].NeighbourList[3] = i + NxAtoms2;	 // up left
					atoms[i].NeighbourList[4] = i + 1 + NxAtoms2; // up
					atoms[i].NeighbourList[5] = i + 2 + NxAtoms2; // up right
				}
			}
			if ((atoms[i].R[1] == SizeY1 + SizeY2) && (atoms[i].R[0] > 0) &&
				(atoms[i].R[0] < SizeX2))
			{												  // [X,SizeY1] side
				atoms[i].NeighbourList[0] = 5;				  // 5 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms2;	 // down left, MATLAB indexing
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms2; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms2; // down right
				atoms[i].NeighbourList[4] = i;				  // left
				atoms[i].NeighbourList[5] = i + 2;			  // right
			}
			if ((atoms[i].R[0] > 0) && (atoms[i].R[0] < SizeX2) &&
				(atoms[i].R[1] > SizeY1) &&
				(atoms[i].R[1] < SizeY1 + SizeY2))
			{												  // all internal atoms
				atoms[i].NeighbourList[0] = 8;				  // 8 neighbours
				atoms[i].NeighbourList[1] = i - NxAtoms2;	 // down left
				atoms[i].NeighbourList[2] = i + 1 - NxAtoms2; // down
				atoms[i].NeighbourList[3] = i + 2 - NxAtoms2; // down right
				atoms[i].NeighbourList[4] = i;				  // left, MATLAB indexing
				atoms[i].NeighbourList[5] = i + 2;			  // right
				atoms[i].NeighbourList[6] = i + NxAtoms2;	 // up left
				atoms[i].NeighbourList[7] = i + 1 + NxAtoms2; // up
				atoms[i].NeighbourList[8] = i + 2 + NxAtoms2; // up right
			}
		}
	}

	// Send out the data back to MATLAB
	nlhs = 1;
	mwSize dims[] = {1, (mwSize)NAtoms};
	const char *field_names[] = {"R", "NeighbourList", "BondList"};
	plhs[0] = mxCreateStructArray(2, dims, 3, field_names);
	double *prR, *prNeighbourList;
	mxArray *array_R, *array_NeighbourList, *array_BondList;
	int name_R = mxGetFieldNumber(plhs[0], "R");
	int name_NeighbourList = mxGetFieldNumber(plhs[0], "NeighbourList");
	int name_BondList = mxGetFieldNumber(plhs[0], "BondList");

	// Populate the output with computed data (mx functions are not thread-safe)
	for (i = 0; i < NAtoms; i++)
	{

		// Create arrays
		array_R = mxCreateDoubleMatrix(1, 2, mxREAL);
		array_NeighbourList = mxCreateDoubleMatrix(1, atoms[i].NeighbourList[0], mxREAL);
		array_BondList = mxCreateDoubleMatrix(1, atoms[i].NeighbourList[0], mxREAL);

		// Get data arrays
		prR = mxGetPr(array_R);
		prNeighbourList = mxGetPr(array_NeighbourList);

		// Populate data arrays
		for (j = 0; j < 2; j++)
			prR[j] = atoms[i].R[j];
		for (j = 0; j < atoms[i].NeighbourList[0]; j++)
			prNeighbourList[j] = atoms[i].NeighbourList[j + 1];

		// Assign arrays to output structure
		mxSetFieldByNumber(plhs[0], i, name_R, array_R);
		mxSetFieldByNumber(plhs[0], i, name_NeighbourList, array_NeighbourList);
		mxSetFieldByNumber(plhs[0], i, name_BondList, array_BondList);
	}

	// Delete atoms database
	delete[] atoms;
}
