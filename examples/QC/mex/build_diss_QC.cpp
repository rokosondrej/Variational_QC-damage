// build_diss_QC: computes the approximate dissipation part of the
// incremental energy of the system

#include <algorithm>
#include <math.h>
#include <stdio.h>

#ifdef _OPENMP
#include <omp.h>
#endif
#include "matrix.h"
#include "mex.h"

/******************* Definition of constants ******************/
const int NP = 100; // initial number of points for the trapezoidal rule
const int NR = 2;   // if relative error of trapezoidal rule is too large, use
                    // NP*NR points in the next iteration

/******************** Function declarations *******************/
int evaluateD(double &D, const double eps, const double E, const double eps0,
              const double epsf, const double R,
              const double TOL); // numerical integration of D'

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  // Test for seven input argument
  if (nrhs != 7)
    mexErrMsgTxt("Seven input arguments required!\n");

  // Get the input data
  double *pr = mxGetPr(prhs[3]);
  double *prp = mxGetPr(prhs[4]);
  double TOL = mxGetScalar(prhs[5]);
  int maxNumThreads = (int)mxGetScalar(prhs[6]);

  // Compute energy, count only terms depending on z_c, z_p
  nlhs = 1;
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  double *prDiss = mxGetPr(plhs[0]);
  int i, status = 0;
  double Diss = 0;
  int NsAtoms = (int)mxGetNumberOfElements(prhs[1]);
  int name_R = mxGetFieldNumber(prhs[0], "R");
  int name_NeighbourList = mxGetFieldNumber(prhs[0], "NeighbourList");
  int name_BondList = mxGetFieldNumber(prhs[0], "BondList");
  int name_ID = mxGetFieldNumber(prhs[1], "ID");
  int name_w = mxGetFieldNumber(prhs[1], "w");
  int name_Potential = mxGetFieldNumber(prhs[2], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
  {
    int _j, _alpha, _beta, _bond, _status = 0;
    double _r1, _r2, _eps1, _eps2, _R, _D1, _D2, _D, _w;
    double *_BondList, *_Potential, *_NeighbourList, *_Ra, *_Rb, *_prw, *_prID;
    double _Rab[2], _rab1[2], _rab2[2];
    mxArray *_pNeighbourList;

#pragma omp for reduction(+ : Diss)
    for (i = 0; i < NsAtoms; i++) { // loop over all sampling atoms
      _prw = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_w));
      _prID = mxGetPr(mxGetFieldByNumber(prhs[1], i, name_ID));
      _w = _prw[0];
      _alpha = (int)_prID[0]; // atom alpha ID, MATLAB indexing

      // Get data for atom alpha
      _pNeighbourList =
          mxGetFieldByNumber(prhs[0], _alpha - 1, name_NeighbourList);
      _NeighbourList = mxGetPr(_pNeighbourList);
      _BondList =
          mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_BondList));
      for (_j = 0; _j < (int)mxGetN(_pNeighbourList);
           _j++) { // loop over all nearest neighbours of sampling atom alpha

        // Get data for the bond between atoms alpha and beta
        _beta = (int)_NeighbourList[_j]; // atom beta ID, MATLAB indexing
        _bond = (int)_BondList[_j];
        _Potential =
            mxGetPr(mxGetFieldByNumber(prhs[2], _bond - 1, name_Potential));

        // Get data for atoms alpha and beta
        _Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
        _Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
        _Rab[0] = _Rb[0] - _Ra[0];
        _Rab[1] = _Rb[1] - _Ra[1];
        _R = sqrt(_Rab[0] * _Rab[0] +
                  _Rab[1] * _Rab[1]); // the initial bond length

        // Previous time step parameters
        _rab1[0] = prp[2 * (_beta - 1)] - prp[2 * (_alpha - 1)];
        _rab1[1] = prp[2 * (_beta - 1) + 1] - prp[2 * (_alpha - 1) + 1];
        _r1 = sqrt(_rab1[0] * _rab1[0] +
                   _rab1[1] * _rab1[1]); // deformed bond length
        _eps1 = (_r1 - _R) / _R;         // bond strain

        // Current time step parameters
        _rab2[0] = pr[2 * (_beta - 1)] - pr[2 * (_alpha - 1)];
        _rab2[1] = pr[2 * (_beta - 1) + 1] - pr[2 * (_alpha - 1) + 1];
        _r2 = sqrt(_rab2[0] * _rab2[0] +
                   _rab2[1] * _rab2[1]); // deformed bond length
        _eps2 = (_r2 - _R) / _R;         // bond strain

        // Evaluate numerically integrals D(eps)
        if (_eps2 > _eps1) {
          _status += evaluateD(_D1, _eps1, _Potential[0], _Potential[1],
                               _Potential[2], _R, TOL);
          _status += evaluateD(_D2, _eps2, _Potential[0], _Potential[1],
                               _Potential[2], _R, TOL);
          _D = _D2 - _D1;

          // Assign D part to the overall energy
          Diss += _w * 0.5 * _D;
        }
      }
    }

// Check convergence of _Di
#pragma omp atomic
    status += _status;
  }

  // Check convergence of _Di
  if (status > 0)
    mexWarnMsgTxt("Integration of Diss did not converge!");

  // Send the data back to MATLAB
  prDiss[0] = Diss;
}

/******************** Function definitions ********************/
// numerical integration of D'
int evaluateD(double &D, const double eps, const double E, const double eps0,
              const double epsf, const double R, const double TOL) {
  double D1 = 0.0, D2 = 0.0, err, eps1, eps2, o1, o2, y1, y2;
  int i, N = NP, maxnp = NP*pow(NR,8)+1;
  int status = 0; // 0 for success
  D = 0.0;

  // Keep refining Simpson's discretization until convergence of eps
  if (eps > eps0) {
    err = 1 + TOL;
    while (
        (err > TOL) &
        (N < maxnp)) { // adaptivity with respect to prescribed relative tolerance
      D2 = 0;
      eps1 = eps0;
      o1 = 1 - (eps0 / eps1) * exp(-(eps1 - eps0) / epsf);
      y1 = 0.5 * E * R * pow(eps1, 2);
      for (i = 0; i < N; i++) { // use the trapezoidal rule
        eps2 = eps1 + (eps - eps0) / N;
        o2 = 1 - (eps0 / eps2) * exp(-(eps2 - eps0) / epsf);
        y2 = 0.5 * E * R * pow(eps2, 2);
        D2 = D2 + 0.5 * (y1 + y2) * (o2 - o1);
        eps1 = eps2;
        o1 = o2;
        y1 = y2;
      }
      err = fabs(D2 - D1) / fabs(D2);
      N = N * NR;
      D1 = D2;
    }
    D = D2;
    if (err > TOL)
      status = 1; // test convergence of numerical integration; 1 for fail
  } else {
    D = 0;
  }

  // Return status
  return status;
}
