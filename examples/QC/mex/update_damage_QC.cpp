// update_damage: compute Z using exponential softening rule

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for six input argument
	if (nrhs != 6)
		mexErrMsgTxt("Six input arguments required!\n");

	// Get the input data
	double *prsampbondsID = mxGetPr(prhs[2]);
	double *prr = mxGetPr(prhs[3]);
	double *promegaP = mxGetPr(prhs[4]);
	int maxNumThreads = (int)mxGetScalar(prhs[5]);

	// Minimeze bond-wise energies
	nlhs = 1;
	int i;
	int NBonds = (int)mxGetNumberOfElements(prhs[1]);
	int NsBonds = (int)mxGetNumberOfElements(prhs[2]);
	plhs[0] = mxCreateDoubleMatrix(NBonds, 1, mxREAL);
	double *omega = mxGetPr(plhs[0]); // current damage
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
	int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _alpha, _beta, _bond;
		double _R, _r, _eps, _omegaP, _temp_o;
		double *_Ra, *_Rb, *_Potential, *_Atoms, _ra[2], _rb[2];

#pragma omp for
		for (i = 0; i < NsBonds; i++)
		{								   // loop over all sampling bonds
			_bond = (int)prsampbondsID[i]; // bond ID, MATLAB indexing
			_omegaP = promegaP[_bond - 1]; // previous time-step damage variable

			// Get data for atoms alpha and beta connected by the bond
			_Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
			_alpha = (int)_Atoms[0]; // atom alpha, MATLAB indexing
			_beta = (int)_Atoms[1];  // atom beta, MATLAB indexing
			_Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
			_Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
			_ra[0] = prr[2 * (_alpha - 1)];
			_ra[1] = prr[2 * (_alpha - 1) + 1];
			_rb[0] = prr[2 * (_beta - 1)];
			_rb[1] = prr[2 * (_beta - 1) + 1];
			_R = sqrt(pow(_Rb[0] - _Ra[0], 2) + pow(_Rb[1] - _Ra[1], 2));
			_r = sqrt(pow(_rb[0] - _ra[0], 2) + pow(_rb[1] - _ra[1], 2)); // bond length

			// Get data for the bond between atoms alpha and beta
			_Potential = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));

			// Damage computation
			_eps = (_r - _R) / _R; // bond strain
			if (_eps > _Potential[1])
			{
				_temp_o = 1 - (_Potential[1] / _eps) * exp(-(_eps - _Potential[1]) / _Potential[2]);
				if (_temp_o < _omegaP)
				{
					_temp_o = _omegaP;
				}
			}
			else
			{ // elastic step
				_temp_o = _omegaP;
			}

#pragma omp atomic
			omega[_bond - 1] += _temp_o;
		}
	}
}
