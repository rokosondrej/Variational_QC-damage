// build_en_QC: computes the approximate elastic part of the incremental
// energy of the system

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"
#include <vector>

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for ten input argument
	if (nrhs != 10)
		mexErrMsgTxt("Ten input arguments required!\n");

	// Get input data
	const double *p = mxGetPr(prhs[0]);
	const double *t = mxGetPr(prhs[1]);
	const mxArray *atoms = prhs[2];
	const mxArray *bonds = prhs[3];
	int Mt = (int)mxGetM(prhs[1]);
	int NTriangles = (int)mxGetN(prhs[1]);

	double *prSbonds = mxGetPr(prhs[4]);
	int nsbonds = (int)std::max(mxGetM(prhs[4]), mxGetN(prhs[4]));
	double *prx = mxGetPr(prhs[5]);
	double *promega = mxGetPr(prhs[6]);
	double theta_refine = mxGetScalar(prhs[7]);
	double TOL = mxGetScalar(prhs[8]);
	// int maxNumThreads = (int)mxGetScalar(prhs[9]);

	int name_atoms = mxGetFieldNumber(bonds, "Atoms");
	int name_Potential = mxGetFieldNumber(bonds, "Potential");
	int name_R = mxGetFieldNumber(atoms, "R");

	// Allocate outputs
	nlhs = 1;
	plhs[0] = mxCreateDoubleMatrix(NTriangles, 1, mxREAL);
	double *tr_to_refine = mxGetPr(plhs[0]);

	// Get triangles to refine
	auto Xs = std::vector<double>{};
	Xs.reserve(nsbonds);
	auto Ys = std::vector<double>{};
	Ys.reserve(nsbonds);
	for (auto i = 0; i < nsbonds; ++i)
	{ // loop over all sampling interactions

		size_t _bond = (size_t)prSbonds[i];
		double *_atoms = mxGetPr(mxGetFieldByNumber(bonds, _bond - 1, name_atoms));
		double *_Potential = mxGetPr(mxGetFieldByNumber(bonds, _bond - 1, name_Potential));
		size_t _alpha = (size_t)_atoms[0]; // atom alpha ID, MATLAB indexing
		size_t _beta = (size_t)_atoms[1];  // atom beta ID, MATLAB indexing

		// Get data for sampling atom alpha
		double _omega = promega[_bond - 1]; // current damage

		double *_Ra = mxGetPr(mxGetFieldByNumber(atoms, _alpha - 1, name_R));
		double _ra[2];
		_ra[0] = prx[2 * (_alpha - 1) + 0];
		_ra[1] = prx[2 * (_alpha - 1) + 1];

		double *_Rb = mxGetPr(mxGetFieldByNumber(atoms, _beta - 1, name_R));
		double _rb[2];
		_rb[0] = prx[2 * (_beta - 1) + 0];
		_rb[1] = prx[2 * (_beta - 1) + 1];

		double _Rab[2];
		_Rab[0] = _Rb[0] - _Ra[0];
		_Rab[1] = _Rb[1] - _Ra[1];

		double _rab[2];
		_rab[0] = _rb[0] - _ra[0];
		_rab[1] = _rb[1] - _ra[1];

		double _R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length
		double _r = sqrt(_rab[0] * _rab[0] + _rab[1] * _rab[1]); // deformed bond length

		// Compute internal part of the energy
		if (_r > _R)
		{
			double _EnInt = (1 - _omega) * 0.5 * (_Potential[0] / _R) * (_r - _R) * (_r - _R);
			double _EnMax = 0.5 * _Potential[0] * _R * _Potential[1] * _Potential[1];
			if (_EnInt > theta_refine * _EnMax)
			{
				Xs.push_back((_Ra[0] + _Rb[0]) / 2.0);
				Ys.push_back((_Ra[1] + _Rb[1]) / 2.0);
			}
		}
	}

	// For each of the critical interactions find in which trinagle it lies
	size_t counter = 0;
	for (auto j = 0; j < NTriangles; ++j)
	{

		// Get triangle's vertices
		double P1[2];
		P1[0] = p[2 * int(t[Mt * j + 0] - 1) + 0];
		P1[1] = p[2 * int(t[Mt * j + 0] - 1) + 1];
		double P2[2];
		P2[0] = p[2 * int(t[Mt * j + 1] - 1) + 0];
		P2[1] = p[2 * int(t[Mt * j + 1] - 1) + 1];
		double P3[2];
		P3[0] = p[2 * int(t[Mt * j + 2] - 1) + 0];
		P3[1] = p[2 * int(t[Mt * j + 2] - 1) + 1];

		// Get triangle's bbox
		double minxe = P1[0];
		double maxxe = P1[0];
		double minye = P1[1];
		double maxye = P1[1];

		minxe = (minxe < P2[0]) ? minxe : P2[0];
		minxe = (minxe < P3[0]) ? minxe : P3[0];
		maxxe = (maxxe > P2[0]) ? maxxe : P2[0];
		maxxe = (maxxe > P3[0]) ? maxxe : P3[0];

		minye = (minye < P2[1]) ? minye : P2[1];
		minye = (minye < P3[1]) ? minye : P3[1];
		maxye = (maxye > P2[1]) ? maxye : P2[1];
		maxye = (maxye > P3[1]) ? maxye : P3[1];

		for (size_t k = 0; k < Xs.size(); ++k)
		{

			// If k-th bond is within triangle's bbox
			if ((Xs[k] > (minxe - TOL)) && (Xs[k] < (maxxe + TOL)) &&
				(Ys[k] > (minye - TOL)) && (Ys[k] < (maxye + TOL)))
			{

				// Test barycentric coordinates
				double alpha = ((P2[1] - P3[1]) * (Xs[k] - P3[0]) +
								(P3[0] - P2[0]) * (Ys[k] - P3[1])) /
							   ((P2[1] - P3[1]) * (P1[0] - P3[0]) +
								(P3[0] - P2[0]) * (P1[1] - P3[1]));
				double beta = ((P3[1] - P1[1]) * (Xs[k] - P3[0]) +
							   (P1[0] - P3[0]) * (Ys[k] - P3[1])) /
							  ((P2[1] - P3[1]) * (P1[0] - P3[0]) +
							   (P3[0] - P2[0]) * (P1[1] - P3[1]));
				double gamma = 1.0 - alpha - beta;

				// If Cg is inside triangle, schedule this triangle for refinement
				if ((alpha > -TOL) && (beta > -TOL) && (gamma > -TOL))
				{
					tr_to_refine[counter] += j + 1; // Matlab indexing
					counter++;
					break;
				}
			}
		}
	}

	// Resize outputs
	mxSetM(plhs[0], counter);
}
