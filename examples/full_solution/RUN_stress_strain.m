%% RUN_L_shaped: script for example in Section 5.1, full lattice

%% Clear workspace
clc; % clear command line
close all;
matlabrc; % restore MATLAB path, etc.
path([pwd,'/mfiles'],path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
spparms('chmodsp') = 1;

%% Input variables

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 'v';

% Geometry: construct an L-shaped region composed of bottom [0,SizeX1]x[0,SizeY1] and top [0,SizeX2]x[0,SizeY2] rectangles
dSize = 1;  % lattice spacing along x- and y-axes

% Material: Potential = [E,eps0,epsf,-]
eps0 = 0.0025;
Potential = [1/eps0,eps0,10*eps0,0];

% Tolerances
TOL_r = 1e-6; % elasticity solver realtive tolerance
TOL_i = 1e-3; % relative tolerance for the numerical integration of D'
TOL_g = 1e-10; % geometric tolerance; a positive number < TOL is treated as zero

%% Construct data structures: atoms and bonds
atoms(1).R = [0,0];
atoms(1).NeighbourList = 2;
atoms(1).BondList = 1;
atoms(2).R = dSize*[1,0];
atoms(2).NeighbourList = 1;
atoms(2).BondList = 1;
R0 = dSize*[0;0;1;0];
bonds(1).Atoms = [1,2];
bonds(1).Potential = Potential;

%% Plot stress-strain curve
Time = [linspace(0,20*eps0,1001),linspace(20*eps0,-eps0,1001)];
R = [R0,zeros(2*length(atoms),length(Time)-1)]; % r for all time steps
Z = zeros(length(bonds),length(Time)); % z for all time steps
REACT = zeros(length(R0),length(Time));
En = zeros(size(Time));
DissDist = zeros(size(Time));
Wext = zeros(size(Time));
for istep = 2:length(Time)
    % Get configuration
    R(:,istep) = R0+Time(istep)*dSize*[0;0;1;0];
    % Get forces and internal variables
    Z(:,istep) = update_damage(atoms,bonds,R(:,istep),Z(:,istep-1),maxNumThreads);
    [REACT(:,istep),I,J,V] = build_grad_hess_r_am(atoms,bonds,R(:,istep),Z(:,istep),maxNumThreads);
    % K = full(sparse(I,J,V,length(R0),length(R0)));
    % Get energy
    En(istep) = build_en(atoms,bonds,Z(:,istep),R(:,istep),maxNumThreads);
    DissDist(istep) = build_diss(atoms,bonds,R(:,istep),R(:,istep-1),TOL_i,maxNumThreads);
    Wext(istep) = Wext(istep-1)+0.5*(REACT(:,istep-1)+...
        REACT(:,istep))'*(R(:,istep)-R(:,istep-1));
end
VarD = cumsum(DissDist); % dissipated energy

%% Plot results
% Energy evolution paths
param = R(3,:)-R0(3);
handle_en = figure(2);clf;hold all;box on;xlabel('strain $\varepsilon$');...
    ylabel('Energy [kJ]');title('MSPlastInterpolSumPlast');
plot(param,En+VarD,'--k','linewidth',1);
plot(param,En,'k');
plot(param,VarD,'-.k');
plot(param,Wext,':k');
legend('$\mathcal{E}+\mathrm{Var}_\mathcal{D}$','$\mathcal{E}$','$\mathrm{Var}_\mathcal{D}$','$\mathcal{W}_\mathrm{ext}$','location','northwest');

% Reaction force
handle_react = figure(3);clf;hold all;box on;xlabel('strain $\varepsilon$');ylabel('$F$');
plot(param,REACT(3,:),'k');

% Internal variable
handle_z = figure(4);clf,hold all;box on;xlabel('strain $\varepsilon$');ylabel('$\omega$');
plot(param,Z,'k');

%% Test consistency by finite differences
% Get configuration
x0 = R0-0.0*rand(size(R0)); % stay within elastic regime
x0(3) = x0(3)+0.05;
y0 = 0.992521569038868;

% Get the reference
D0 = build_diss(atoms,bonds,x0,R0,TOL_i,maxNumThreads);
[F,I,J,V] = build_grad_hess_r_consistent(atoms,bonds,x0,y0,maxNumThreads);
K = full(sparse(I,J,V,length(R0),length(R0)));

K

% Plot one stiffness component as a derivative of the reaction force
handle_k = figure(5);clf,hold all;box on;xlabel('strain $\varepsilon$');ylabel('$C_{11}$');
plot(param,gradient(REACT(3,:),param));
