%% RUN_four_point_bending: script for example in Section 5.2 (antisymmetric four-point bending test), full lattice solution

%% Clear workspace
clc; % clear command line
close all; % close all figures
matlabrc; % restore MATLAB path, etc.
path([pwd,'/mfiles'],path); % add path to mfiles folder containing all m-files
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
spparms('chmodsp') = 1;

%% Input variables

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 2;

% Geometry: construct a rectangle [-SizeX,SizeX]x[-SizeY,SizeY]
dSize = 1; % lattice spacing along x- and y-axes
SizeH = 512*dSize; % beam length, all remaining parameters are derived from it
SizeX = 2*round(SizeH/4); % half domain size along x-axis
SizeY = 1/4*round(SizeX); % half domain size along y-axis
SizeS2 = 2*round(SizeX/11/2); % x-distance for a support/force
SizeS1 = SizeX-SizeS2; % x-distance for a support/force
SizeS = SizeS1+SizeS2; % distance between supports/forces
NotchX = 4*dSize; % half size of the notch along x-axis
NotchY = SizeS2; % initial notch depth

% Material: Potential = [E,eps0,epsf,-]
Potential = [1,0.01,0.025,0]; % for the whole domain
PotentialP = [1000,100,0.25,0]; % for stiff pads

% Stiff pad under DBCs (its size along x-axis)
Npad = ceil(SizeS2/dSize/2);

% Tolerances
TOL_r = 1e-6; % elasticity solver relative tolerance
TOL_i = 1e-3; % relative tolerance for the numerical integration of D'
TOL_g = 1e-10; % geometric tolerance; a positive number < TOL is treated as zero

%% Assembly initial data
fprintf('Assembling initial data...\n'),tic;
atoms = build_atoms_4([SizeX,SizeY,NotchX,NotchY,dSize,dSize],TOL_g,maxNumThreads);
R0 = [atoms(:).R]';
bonds = build_bonds_4(atoms,[SizeX,SizeY,NotchX,NotchY,dSize,dSize],...
    Potential,maxNumThreads);

% Stiffen locally interactions near prescribed BC
IDPad1 = find(abs(R0(1:2:end)+SizeS2)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
IDPad2 = find(abs(R0(1:2:end)-SizeS1)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
IDPad3 = find(abs(R0(1:2:end)+SizeS1)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);
IDPad4 = find(abs(R0(1:2:end)-SizeS2)<Npad*dSize+TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);
IDPad = unique([IDPad1,IDPad2,IDPad3,IDPad4]);

% Stiffen locally bonds linked to Pad atoms and to their neighbours
PadNeigh = [atoms(IDPad).NeighbourList];
idPadBonds = unique([atoms(PadNeigh).BondList]);
for i = 1:length(idPadBonds)
    bonds(idPadBonds(i)).Potential = PotentialP;
end

%% Prescribe boundary conditions BC: 0 - no BC, 1 - Dirichlet BC type

% Fixed points
id1 = find(abs(R0(1:2:end)+SizeS2)<TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
id2 = find(abs(R0(1:2:end)-SizeS1)<TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);

% Vertical forces
id3 = find(abs(R0(1:2:end)+SizeS1)<TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);
id4 = find(abs(R0(1:2:end)-SizeS2)<TOL_g &...
    abs(R0(2:2:end)+SizeY)<TOL_g);

% Construct C vector for CMOD+CMSD control
id5 = find(abs(R0(1:2:end)+NotchX)<TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
id6 = find(abs(R0(1:2:end)-NotchX)<TOL_g &...
    abs(R0(2:2:end)-SizeY)<TOL_g);
C = zeros(size(R0));
C(2*id5-1) = -1;
C(2*id6-1) = 1;
C(2*id5) = -1;
C(2*id6) = 1;

% Extract code numbers
DBCIndices = [2*id1-1;2*id1;2*id2]; % code numbers for constrained repatoms
DBCValues = 0*DBCIndices; % prescribed displacements for constrained repatoms
FreeIndices = setdiff(1:2*length(atoms),DBCIndices)'; % all free repatoms
Fext = zeros(size(R0));
Fext([2*id3,2*id4]) = 1/SizeS*[SizeS2,SizeS1];
Fext = Fext/norm(Fext);

%% Solve for the evolution process of the system
fprintf('Solving...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., CMOD+CMSD = %g, %g s\n',1,0,0,0);
timeIter = [0,0,0];
R = [R0,zeros(2*length(atoms),2)]; % r for all time steps
Z = sparse(length(bonds),3); % z for all time steps
i = 2;
Lambda = [0,0,0];

% Indirect displacement control constant
Larc = 0.05;

while C'*(R(:,i-1)-R0) < 5.5
    
    % Indirect displacement control constant
    if i>7
        Larc = 0.01;
    end
    if i>377
        Larc = 0.05;
    end
    t_start_2 = tic;
    
    % MINIMIZE THE REDUCED ENERGY WITH RESPECT TO r
    [r2,Niter,Lambda(i)] = minimize_r(atoms,bonds,R(:,i-1),DBCIndices,...
        DBCValues,Fext,FreeIndices,R0,full(Z(:,i-1)),C,TOL_r,...
        Larc,Lambda(i-1),maxNumThreads);
    
    % MINIMIZE WITH RESPECT TO z
    z2 = update_damage(atoms,bonds,r2,full(Z(:,i-1)),maxNumThreads);
    
    % Store converged minimizing values
    R(:,i) = r2;
    Z(:,i) = z2;
    
    % Print iteration message
    fprintf('%d step, %d Nwtn it., CMOD+CMSD = %g, %g s\n',i,Niter,...
        C'*(R(:,i)-R0),toc(t_start_2));
    timeIter(i) = Niter;
    
    % Proceed to the next time step
    i = i+1;
    
    % Plot system's current configuration
    figure(1),clf,hold all,axis equal;
    plot(r2(1:2:end),r2(2:2:end),'.k');
    drawnow;
end

% Print solution statistics
fprintf('\nE[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n',toc(t_start_1));

%% Draw results
% Plot atoms in deformed configuration
scale = 10;
step = length(Lambda);
figure(1),clf,hold all,axis equal,title(['Deformed structure, scale = ',...
    num2str(scale)]),xlabel('x'),ylabel('y');
plot(R0(1:2:end)+scale*(R(1:2:end,step)-R0(1:2:end)),...
    R0(2:2:end)+scale*(R(2:2:end,step)-R0(2:2:end)),'.k','MarkerSize',5);

% Compute reactions
REACT = zeros(size(R));
for i = 2:length(Lambda)
    REACT(:,i) = build_grad_r(atoms,bonds,R(:,i),full(Z(:,i)),maxNumThreads);
end

% Compute final energy evolutions
En = zeros(size(Lambda)); % elastic energy
DissDist = zeros(size(Lambda)); % dissipation distances
Wext = zeros(1,length(Lambda)); % work done by external forces
for i = 2:length(Lambda)
    En(i) = build_en(atoms,bonds,full(Z(:,i)),R(:,i),maxNumThreads);
    DissDist(i) = build_diss(atoms,bonds,R(:,i),R(:,i-1),TOL_i,maxNumThreads);
    Wext(i) = Wext(i-1)+0.5*(Lambda(i-1)+Lambda(i))*...
        Fext'*(R(:,i)-R(:,i-1));
end
VarD = cumsum(DissDist); % dissipated energy

% Plot energy evolution paths
param = C'*R-C'*R0;
figure(2),clf,hold all,box on,xlabel('time step [-]'),...
    ylabel('Energy [kJ]'),title('MSPlastInterpolSumPlast');
plot(param,En+VarD,'--k','linewidth',1);
plot(param,En,'k');
plot(param,VarD,'-.k');
plot(param,Wext,':k');
legend('E+Var_D','E','Var_D','W_{ext}','location','northwest');

% Plot reaction force
figure(3),clf,hold all,box on,xlabel('\lambda(t)'),ylabel('F');
plot(param,Lambda,'k');

%% Export data to Paraview
U = zeros(size(R0));
for istep = 1:length(Lambda)
    U(:,istep) = R(:,istep)-R0;
end
ParaView_export_2d('full_4',atoms,bonds,R0,U,Z,Lambda,maxNumThreads);
