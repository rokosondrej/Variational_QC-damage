function [r2,Niter,lambda] = minimize_r(atoms,bonds,r1,DBCIndices,...
    DBCValues,F,FreeIndices,R0,z1,C,TOL_r,Larc,lambda,maxNumThreads)

% Solve for x - a vector of free degrees of freedom, using standard Newton algorithm
x = r1(FreeIndices);
eps_r = 1+TOL_r;
Niter = 0;
C(DBCIndices) = [];
F = F(FreeIndices);
while eps_r > TOL_r
    Niter = Niter+1;
    [r2,f_r,K_r,f_rDBC,K_rDBC] = grad_hess(atoms,bonds,x,DBCIndices,...
        lambda*DBCValues,FreeIndices,R0,z1,maxNumThreads);
    
    % Compute individual increments
    lhs = -K_r\[f_r-lambda*F,K_rDBC*DBCValues-F];
    dxt = lhs(:,1);
    dxF = lhs(:,2);
    
    % Parameters for the indirect displacement control method
    dlambda = (Larc-C'*(x-r1(FreeIndices)+dxt))/(C'*dxF);
    
    % Construct the increment
    dx = dxt+dlambda*dxF;
    lambda = lambda+dlambda;
    x = x+dx;
    
    % Update the error
    eps_r = norm(dx,'inf')./norm(x-R0(FreeIndices),'inf');
    if norm(f_rDBC,'inf')>0
        eps_r = eps_r + norm(f_r-lambda*F,'inf')/norm(f_rDBC,'inf');
    end
end

% Reconstruct converged r-vector from x
r2(DBCIndices) = R0(DBCIndices)+lambda*DBCValues;
r2(FreeIndices) = x;

end
