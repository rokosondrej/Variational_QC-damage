function [r2,f_r,K_r,f_rDBC,K_rDBC] = grad_hess(atoms,bonds,x,DBCIndices,...
    tDBCValues,FreeIndices,R0,z,maxNumThreads)

% Reconstruct r2
r2 = zeros(2*length(atoms),1);
r2(DBCIndices) = R0(DBCIndices)+tDBCValues;
r2(FreeIndices) = x;

% Compute gradient and Hessian
% [f_r,I,J,S] = build_grad_hess_r_consistent(atoms,bonds,r2,z,maxNumThreads);
[f_r,I,J,S] = build_grad_hess_r_secant(atoms,bonds,r2,z,maxNumThreads);
f_rDBC = f_r(DBCIndices);
f_r = f_r(FreeIndices);
K_r = sparse(I(:),J(:),S(:),length(r2),length(r2));
K_rDBC = K_r(FreeIndices,DBCIndices);
K_r = K_r(FreeIndices,FreeIndices);

end
