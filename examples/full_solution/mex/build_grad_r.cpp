// build_grad_r: assembly the gradient of internal energy w.r.t. r

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for five input arguments
	if (nrhs != 5)
		mexErrMsgTxt("Five input arguments required!\n");

	// Get the input data
	double *prx = mxGetPr(prhs[2]);
	double *promega = mxGetPr(prhs[3]);
	int maxNumThreads = (int)mxGetScalar(prhs[4]);
	int NAtoms = (int)mxGetNumberOfElements(prhs[0]);
	int NBonds = (int)mxGetNumberOfElements(prhs[1]);

	// Compute the gradient and send out the data back to MATLAB
	nlhs = 1;
	mxArray *Grad = mxCreateDoubleMatrix(2 * NAtoms, 1, mxREAL);
	double *prGrad = mxGetPr(Grad);
	plhs[0] = Grad;
	int i;
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
	int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _bond, _alpha, _beta;
		double _R, _r, _dphi, _omega;
		double *_Ra, *_Rb, *_Atoms, *_Potential;
		double _rab[2], _Rab[2], _fab[2];

#pragma omp for
		for (i = 0; i < NBonds; i++)
		{				   // loop over all atoms
			_bond = i + 1; // bond ID, MATLAB indexing

			// Get data for the bond
			_Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
			_alpha = (int)_Atoms[0];
			_beta = (int)_Atoms[1];
			_Potential = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));
			_omega = promega[_bond - 1]; // damage variable omega

			// Get data for atoms associated with current bond
			_Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
			_Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
			_Rab[0] = _Rb[0] - _Ra[0];
			_Rab[1] = _Rb[1] - _Ra[1];
			_rab[0] = prx[2 * (_beta - 1)] - prx[2 * (_alpha - 1)];
			_rab[1] = prx[2 * (_beta - 1) + 1] - prx[2 * (_alpha - 1) + 1];
			_R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length
			_r = sqrt(_rab[0] * _rab[0] + _rab[1] * _rab[1]); // deformed bond length

			// Compute dphi = \phi'
			if (_r > _R)
			{
				_dphi = (1 - _omega) * (_Potential[0] / _R) * (_r - _R);
			}
			else
			{
				_dphi = (_Potential[0] / _R) * (_r - _R);
			}

			// Compute force components
			_fab[0] = -_dphi * _rab[0] / _r;
			_fab[1] = -_dphi * _rab[1] / _r;

			// Allocate fa to Grad
#pragma omp atomic
			prGrad[2 * (_alpha - 1)] += _fab[0];
#pragma omp atomic
			prGrad[2 * (_alpha - 1) + 1] += _fab[1];

			// Allocate fa to Grad force acting on beta
#pragma omp atomic
			prGrad[2 * (_beta - 1)] -= _fab[0];
#pragma omp atomic
			prGrad[2 * (_beta - 1) + 1] -= _fab[1];
		}
	}
}
