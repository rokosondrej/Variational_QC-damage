// build_grad_hess_r: assembly the gradient and Hessian of internal energy
// w.r.t. r

#include <algorithm>
#include <math.h>
#include <stdio.h>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "matrix.h"
#include "mex.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  // Test for five input arguments
  if (nrhs != 5)
    mexErrMsgTxt("Five input arguments required!\n");

  // Get the input data
  double *prx = mxGetPr(prhs[2]);
  double *promega = mxGetPr(prhs[3]);
  int maxNumThreads = (int)mxGetScalar(prhs[4]);
  int NAtoms = (int)mxGetNumberOfElements(prhs[0]);
  int NBonds = (int)mxGetNumberOfElements(prhs[1]);

  // Allocate outputs
  nlhs = 4;
  plhs[0] = mxCreateDoubleMatrix(2 * NAtoms, 1, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(16, NBonds, mxREAL);
  plhs[2] = mxCreateDoubleMatrix(16, NBonds, mxREAL);
  plhs[3] = mxCreateDoubleMatrix(16, NBonds, mxREAL);
  double *prGrad = mxGetPr(plhs[0]);
  double *prI = mxGetPr(plhs[1]);
  double *prJ = mxGetPr(plhs[2]);
  double *prS = mxGetPr(plhs[3]);

  // Compute the gradient and Hessian, populate prGrad, prI, prJ, prS
  int name_R = mxGetFieldNumber(prhs[0], "R");
  int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
  int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
  {
    int _m, _n, _bond, _alpha, _beta;
    double _R, _r, _r2, _r3, _dphi, _ddphi, _omega, _eps;
    double _dphi2_r, _dphi2_r3, _ddphi2_r2;
    double *_Ra, *_Rb, *_Atoms, *_Potential;
    double _rab[2], _Rab[2], _fab[2];
    double _Kbond[4][4]; // local stiffness matrix for a single bond
    int _Ln[4];          // vector of localization numbers

#pragma omp for
    for (int i = 0; i < NBonds; i++)
    {                // loop over all bonds
      _bond = i + 1; // bond ID, MATLAB indexing

      // Get data for the bond
      _Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
      _alpha = (int)_Atoms[0];
      _beta = (int)_Atoms[1];
      _Potential =
          mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));
      _omega = promega[_bond - 1]; // previous time step damage variable

      // Get data for atoms associated with current bond
      _Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
      _Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
      _Rab[0] = _Rb[0] - _Ra[0];
      _Rab[1] = _Rb[1] - _Ra[1];
      _rab[0] = prx[2 * (_beta - 1)] - prx[2 * (_alpha - 1)];
      _rab[1] = prx[2 * (_beta - 1) + 1] - prx[2 * (_alpha - 1) + 1];
      _R = sqrt(_Rab[0] * _Rab[0] +
                _Rab[1] * _Rab[1]); // the initial bond length
      _r2 = _rab[0] * _rab[0] + _rab[1] * _rab[1];
      _r = sqrt(_r2); // deformed bond length
      _r3 = _r2 * _r;

      /*<<<<<<<<<<<<<<<<<<<<<<<< Build grad >>>>>>>>>>>>>>>>>>>>>>>>*/
      // Compute dphi = \phi', and ddphi = \phi''
      _eps = (_r - _R) / _R; // bond strain
      if (_eps > 0.0)
      {
        _dphi = (1 - _omega) * (_Potential[0] / _R) * (_r - _R);
        _ddphi = (1 - _omega) * _Potential[0] / _R;
      }
      else
      {
        _dphi = (_Potential[0] / _R) * (_r - _R);
        _ddphi = _Potential[0] / _R;
      }

      // Compute force components
      _fab[0] = -_dphi * _rab[0] / _r;
      _fab[1] = -_dphi * _rab[1] / _r;

      // Allocate fab to Grad, force acting on alpha
#pragma omp atomic
      prGrad[2 * (_alpha - 1)] += _fab[0];
#pragma omp atomic
      prGrad[2 * (_alpha - 1) + 1] += _fab[1];

      // Allocate fa to Grad, force acting on atom beta
#pragma omp atomic
      prGrad[2 * (_beta - 1)] -= _fab[0];
#pragma omp atomic
      prGrad[2 * (_beta - 1) + 1] -= _fab[1];

      /*<<<<<<<<<<<<<<<<<<<<<<<< Build Hess >>>>>>>>>>>>>>>>>>>>>>>>*/
      // Construct Kbond - hessian matrix of a single bond
      // gamma = alpha, delta = alpha
      _dphi2_r = _dphi / _r;
      _dphi2_r3 = _dphi / _r3;
      _ddphi2_r2 = _ddphi / _r2;
      // Part corresponding to the branch that is loading/unloading elastically
      _Kbond[0][0] = (_dphi2_r + (_ddphi2_r2 - _dphi2_r3) * (_rab[0] * _rab[0]));
      _Kbond[0][1] = ((_ddphi2_r2 - _dphi2_r3) * (_rab[0] * _rab[1]));
      _Kbond[1][0] = ((_ddphi2_r2 - _dphi2_r3) * (_rab[1] * _rab[0]));
      _Kbond[1][1] = (_dphi2_r + (_ddphi2_r2 - _dphi2_r3) * (_rab[1] * _rab[1]));
      // gamma = beta, delta = beta
      _Kbond[2][2] = _Kbond[0][0];
      _Kbond[2][3] = _Kbond[0][1];
      _Kbond[3][2] = _Kbond[1][0];
      _Kbond[3][3] = _Kbond[1][1];
      // gamma = alpha, delta = beta
      _Kbond[0][2] = -_Kbond[0][0];
      _Kbond[0][3] = -_Kbond[0][1];
      _Kbond[1][2] = -_Kbond[1][0];
      _Kbond[1][3] = -_Kbond[1][1];
      // gamma = beta, delta = alpha
      _Kbond[2][0] = _Kbond[0][2];
      _Kbond[2][1] = _Kbond[0][3];
      _Kbond[3][0] = _Kbond[1][2];
      _Kbond[3][1] = _Kbond[1][3];

      // Allocate Kbond into prI, prJ, prS, the COO representation of the global stiffness matrix
      _Ln[0] = 2 * _alpha - 1; // indexing for MATLAB
      _Ln[1] = 2 * _alpha;
      _Ln[2] = 2 * _beta - 1;
      _Ln[3] = 2 * _beta;

#pragma omp critical(ALLOCATE)
      {
        for (_n = 0; _n < 4; _n++)
        {
          for (_m = 0; _m < 4; _m++)
          {
            prI[(_bond - 1) * 16 + _n * 4 + _m] = _Ln[_m];
            prJ[(_bond - 1) * 16 + _n * 4 + _m] = _Ln[_n];
            prS[(_bond - 1) * 16 + _n * 4 + _m] += _Kbond[_m][_n];
          }
        }
      }
    }
  }
}
