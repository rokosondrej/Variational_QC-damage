// build_bonds: builds a database of all bonds, updates BondLists of
// atoms database.
// Implementation below exploits specific numbering of atoms from [0,0] to
// [SizeX2,SizeY1+SizeY2].

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/******************** Structure declarations ******************/
struct BOND
{
	int ID;		  // ID of a bond
	int Atoms[2]; // atoms connected through this bond
};

/******************** Function declarations *******************/
int distID(const void *BondA, const void *BondB);	// comparison function w.r.t. ID
int distAtom1(const void *BondA, const void *BondB); // comparison function w.r.t. first atom
int distAtom2(const void *BondA, const void *BondB); // comparison function w.r.t. second atom

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for six input argument
	if (nrhs != 6)
		mexErrMsgTxt("Six input arguments required!\n");

	// Copy input variables
	double *prRect = mxGetPr(prhs[1]);
	if ((int)mxGetN(prhs[1]) != 6)
	{
		printf("\nInput vector length must equal to 6.\n");
		return;
	}
	double SizeX1 = prRect[0];
	double SizeY1 = prRect[1];
	double SizeX2 = prRect[2];
	double SizeY2 = prRect[3];
	double dSizeX = prRect[4];
	double dSizeY = prRect[5];
	int NAtoms = (int)mxGetNumberOfElements(prhs[0]);
	double *Potential1 = mxGetPr(prhs[2]);
	double *Potential2 = mxGetPr(prhs[3]);
	if ((int)mxGetN(prhs[2]) != 4 || (int)mxGetN(prhs[3]) != 4)
	{
		printf("\nInput vector specifying potentials must be of length 4!\n");
		return;
	}
	double TOL = mxGetScalar(prhs[4]); // a positive distance < TOL is treated as zero
	int maxNumThreads = (int)mxGetScalar(prhs[5]);

	// Initialize the data
	int i, j;
	int NxAtoms1 = (int)floor(SizeX1 / dSizeX) + 1;															   // number of atoms in x-direction, bottom rectangle
	int NyAtoms1 = (int)floor(SizeY1 / dSizeY);																   // number of atoms in y-direction, bottom rectangle
	int NxAtoms2 = (int)floor(SizeX2 / dSizeX) + 1;															   // number of atoms in x-direction, upper rectangle
	int NyAtoms2 = (int)floor(SizeY2 / dSizeY) + 1;															   // number of atoms in y-direction, upper rectangle
	int NAtoms1 = NxAtoms1 * NyAtoms1;																		   // number of atoms in bottom rectangle
	int NBonds1 = NyAtoms1 * (NxAtoms1 - 1) + NxAtoms1 * (NyAtoms1 - 1) + 2 * (NyAtoms1 - 1) * (NxAtoms1 - 1); // bonds in bottom rectangle
	int NBonds2 = NyAtoms2 * (NxAtoms2 - 1) + NxAtoms2 * (NyAtoms2 - 1) + 2 * (NyAtoms2 - 1) * (NxAtoms2 - 1); // bonds in upper rectangle
	int NBonds12 = NxAtoms1 + 2 * (NxAtoms1 - 1);															   // bonds connecting both rectangles
	int NBonds = NBonds1 + NBonds2 + NBonds12;
	BOND *BondsA = new BOND[NBonds]; // bonds database
	BOND *BondsB = new BOND[NBonds]; // working bonds database
	if (BondsA == NULL || BondsB == NULL)
	{
		printf("\nNot enough memory for Bonds!\n");
		return;
	}

	// Build Bonds for bottom rectangle
	// Horizontal bonds
	int counter = 0;
	for (j = 0; j < NyAtoms1; j++)
	{
		for (i = 0; i < (NxAtoms1 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = i + 1 + j * NxAtoms1;
			BondsA[counter].Atoms[1] = i + 2 + j * NxAtoms1;
			counter++;
		}
	}

	// Vertical bonds
	for (j = 0; j < NxAtoms1; j++)
	{
		for (i = 0; i < (NyAtoms1 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = j + 1 + i * NxAtoms1;
			BondsA[counter].Atoms[1] = j + 1 + (i + 1) * NxAtoms1;
			counter++;
		}
	}

	// Ascending diagonals
	for (j = 0; j < (NyAtoms1 - 1); j++)
	{
		for (i = 0; i < (NxAtoms1 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = i + 1 + j * NxAtoms1;
			BondsA[counter].Atoms[1] = i + NxAtoms1 + 2 + j * NxAtoms1;
			counter++;
		}
	}

	// Descending diagonals
	for (j = 0; j < (NyAtoms1 - 1); j++)
	{
		for (i = 0; i < (NxAtoms1 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = i + 1 + NxAtoms1 + j * NxAtoms1;
			BondsA[counter].Atoms[1] = i + 2 + j * NxAtoms1;
			counter++;
		}
	}

	// Build Bonds connection bottom and upper rectangle
	// Vertical bonds
	for (i = 0; i < NxAtoms1; i++)
	{
		BondsA[counter].ID = counter + 1; // MATLAB indexing
		BondsA[counter].Atoms[0] = i + 1 + NAtoms1 - NxAtoms1;
		BondsA[counter].Atoms[1] = i + 1 + NAtoms1;
		counter++;
	}

	// Ascending diagonals
	for (i = 0; i < (NxAtoms1 - 1); i++)
	{
		BondsA[counter].ID = counter + 1; // MATLAB indexing
		BondsA[counter].Atoms[0] = i + 1 + NAtoms1 - NxAtoms1;
		BondsA[counter].Atoms[1] = i + 2 + NAtoms1;
		counter++;
	}

	// Descending diagonals
	for (i = 0; i < (NxAtoms1 - 1); i++)
	{
		BondsA[counter].ID = counter + 1; // MATLAB indexing
		BondsA[counter].Atoms[0] = i + 1 + NAtoms1;
		BondsA[counter].Atoms[1] = i + 2 + NAtoms1 - NxAtoms1;
		counter++;
	}

	// Build Bonds databases for upper rectangle
	// Horizontal bonds
	for (j = 0; j < NyAtoms2; j++)
	{
		for (i = 0; i < (NxAtoms2 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = NAtoms1 + i + 1 + j * NxAtoms2;
			BondsA[counter].Atoms[1] = NAtoms1 + i + 2 + j * NxAtoms2;
			counter++;
		}
	}

	// Vertical bonds
	for (j = 0; j < NxAtoms2; j++)
	{
		for (i = 0; i < (NyAtoms2 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = NAtoms1 + j + 1 + i * NxAtoms2;
			BondsA[counter].Atoms[1] = NAtoms1 + j + 1 + (i + 1) * NxAtoms2;
			counter++;
		}
	}

	// Ascending diagonals
	for (j = 0; j < (NyAtoms2 - 1); j++)
	{
		for (i = 0; i < (NxAtoms2 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = NAtoms1 + i + 1 + j * NxAtoms2;
			BondsA[counter].Atoms[1] = NAtoms1 + i + NxAtoms2 + 2 + j * NxAtoms2;
			counter++;
		}
	}

	// Descending diagonals
	for (j = 0; j < (NyAtoms2 - 1); j++)
	{
		for (i = 0; i < (NxAtoms2 - 1); i++)
		{
			BondsA[counter].ID = counter + 1; // MATLAB indexing
			BondsA[counter].Atoms[0] = NAtoms1 + i + 1 + NxAtoms2 + j * NxAtoms2;
			BondsA[counter].Atoms[1] = NAtoms1 + i + 2 + j * NxAtoms2;
			counter++;
		}
	}

	// Copy BondsA to BondsB
	for (i = 0; i < NBonds; i++)
	{
		BondsB[i].ID = BondsA[i].ID;
		BondsB[i].Atoms[0] = BondsA[i].Atoms[0];
		BondsB[i].Atoms[1] = BondsA[i].Atoms[1];
	}

	// Sort both databases w.r.t. first and secod atoms
	if (maxNumThreads > 1)
	{
#pragma omp parallel sections
		{
#pragma omp section
			{
				qsort(BondsA, NBonds, sizeof(BOND), distAtom1); // sort w.r.t. the first atom ID
			}
#pragma omp section
			{
				qsort(BondsB, NBonds, sizeof(BOND), distAtom2); // sort w.r.t. the second atom ID
			}
		}
	}
	else
	{
		qsort(BondsA, NBonds, sizeof(BOND), distAtom1); // sort w.r.t. the first atom ID
		qsort(BondsB, NBonds, sizeof(BOND), distAtom2); // sort w.r.t. the second atom ID
	}

	// Create vectors of multiplicity
	int *multA = new int[NAtoms];
	int m = 0;
	for (i = 0; i < NAtoms; i++)
	{ // for first atoms in BondsA
		j = 0;
		while (BondsA[m].Atoms[0] == (i + 1))
		{ // MATLAB indexing
			j++;
			m++;
		}
		multA[i] = j;
	}
	int *multB = new int[NAtoms];
	m = 0;
	for (i = 0; i < NAtoms; i++)
	{ // for first atoms in BondsB
		j = 0;
		while (BondsB[m].Atoms[1] == (i + 1))
		{ // MATLAB indexing
			j++;
			m++;
		}
		multB[i] = j;
	}

	// Create vectors of cummulative sums
	int *cumsumA = new int[NAtoms];
	int *cumsumB = new int[NAtoms];
	cumsumA[0] = multA[0];
	cumsumB[0] = multB[0];
	for (i = 1; i < NAtoms; i++)
	{
		cumsumA[i] = cumsumA[i - 1] + multA[i];
		cumsumB[i] = cumsumB[i - 1] + multB[i];
	}

	// For all atoms and all neighbours, find ID of their bonds and store the data
	double *NeighbourList, *BondList;
	bool foundA, foundB;
	int alpha, beta, tempMin, tempMax;
	mxArray *pBondList;
	int name_NeighbourList = mxGetFieldNumber(prhs[0], "NeighbourList");
	int name_BondList = mxGetFieldNumber(prhs[0], "BondList");
	for (i = 0; i < NAtoms; i++)
	{				   // for cycle over all atoms
		alpha = i + 1; // ID of an atom alpha

		// Get data for an i-th atom, atom alpha
		NeighbourList = mxGetPr(mxGetFieldByNumber(prhs[0], alpha - 1, name_NeighbourList));
		pBondList = mxGetFieldByNumber(prhs[0], alpha - 1, name_BondList);
		BondList = mxGetPr(pBondList);
		for (j = 0; j < (int)mxGetN(pBondList); j++)
		{								  // for cycle over all nearest neighbours
			beta = (int)NeighbourList[j]; // ID of atom beta, MATLAB indexing

			// Find in BondsA, BondsB ID of bond among atoms alpha and beta;
			// we know that any two atoms are connected only by one bond
			foundA = false;
			foundB = false;
			counter = 0;
			if (alpha == 1)
			{
				tempMin = 0;
				tempMax = cumsumA[(alpha - 1)];
			}
			else
			{
				tempMin = cumsumA[(alpha - 1) - 1];
				tempMax = cumsumA[(alpha - 1)];
			}
			for (m = tempMin; m < tempMax; m++)
			{ // MATLAB indexing
				if (BondsA[m].Atoms[1] == beta)
				{
					foundA = true;
					counter = m;
					break;
				}
			}
			if (foundA == false)
			{ // if not found, then reverse the order beta and alpha
				if (alpha == 1)
				{
					tempMin = 0;
					tempMax = cumsumB[(alpha - 1)];
				}
				else
				{
					tempMin = cumsumB[(alpha - 1) - 1];
					tempMax = cumsumB[(alpha - 1)];
				}
				for (m = tempMin; m < tempMax; m++)
				{ // MATLAB indexing
					if (BondsB[m].Atoms[0] == beta)
					{
						foundB = true;
						counter = m;
						break;
					}
				}
			}
			if (foundA == true)
			{
				BondList[j] = (double)BondsA[counter].ID; // store found ID to BondList
			}
			else if (foundB == true)
			{
				BondList[j] = (double)BondsB[counter].ID; // store found ID to BondList
			}
			else
			{
				printf("Unable to find bond for atoms %u and %u.\n", alpha, beta);
			}
		}
	}
	delete[] multA;
	delete[] multB;
	delete[] cumsumA;
	delete[] cumsumB;
	delete[] BondsB;

	// Sort resulting BondsA database by IDs
	qsort(BondsA, NBonds, sizeof(BOND), distID);

	// Construct bonds database and send out the data back to MATLAB
	nlhs = 1;
	mwSize dims[] = {1, (mwSize)NBonds};
	const char *field_names[] = {"Atoms", "Potential"};
	plhs[0] = mxCreateStructArray(2, dims, 2, field_names);

	// Populate output with computed data: mx functions are not thread-safe
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_Atoms = mxGetFieldNumber(plhs[0], "Atoms");
	int name_Potential = mxGetFieldNumber(plhs[0], "Potential");
	for (i = 0; i < NBonds; i++)
	{
		mxArray *array_Atoms, *array_Potential;
		double *prAtoms, *prPotential;

		// Create arrays
		array_Atoms = mxCreateDoubleMatrix(1, 2, mxREAL);
		array_Potential = mxCreateDoubleMatrix(1, 4, mxREAL);

		// Get data arrays
		prAtoms = mxGetPr(array_Atoms);
		prPotential = mxGetPr(array_Potential);

		// And populate them
		for (j = 0; j < 2; j++)
		{
			prAtoms[j] = BondsA[i].Atoms[j];
		}

		/*<<<<<<<<<<<<<<<<<<<<<<<< Assign material properties >>>>>>>>>>>>>>>>>>>>>>>>*/
		// Get data for atoms alpha and beta
		alpha = BondsA[i].Atoms[0];											  // bond's atom alpha, MATLAB indexing
		beta = BondsA[i].Atoms[1];											  // bond's atom beta, MATLAB indexing
		double *Ra = mxGetPr(mxGetFieldByNumber(prhs[0], alpha - 1, name_R)); // initial coordinates of an atom alpha
		double *Rb = mxGetPr(mxGetFieldByNumber(prhs[0], beta - 1, name_R));  // initial coordinates of an atom beta

		// If atoms alpha and beta are within the bottom rectangle
		if ((Ra[1] < SizeY1 - TOL) || (Rb[1] < SizeY1 - TOL))
		{

			// Assign parameters stored in PotentialB
			for (j = 0; j < 4; j++)
				prPotential[j] = Potential1[j];
		}

		// If alpha and beta are not within the bottom subdomain
		else
		{

			// Assign parameters stored in PotentialU
			for (j = 0; j < 4; j++)
				prPotential[j] = Potential2[j];
		}

		// Assign output arrays
		mxSetFieldByNumber(plhs[0], i, name_Atoms, array_Atoms);
		mxSetFieldByNumber(plhs[0], i, name_Potential, array_Potential);
	}
	delete[] BondsA;
}

/******************** Function definitions ********************/
// Comparison function w.r.t. ID
int distID(const void *BondA, const void *BondB)
{
	BOND *A = (BOND *)BondA,
		 *B = (BOND *)BondB;
	double r;
	r = A->ID - B->ID;
	if (r < 0)
		return -1;
	else if (r > 0)
		return 1;
	else
		return 0;
}

// Comparison function w.r.t. first atom
int distAtom1(const void *BondA, const void *BondB)
{
	BOND *A = (BOND *)BondA,
		 *B = (BOND *)BondB;
	double r;
	r = A->Atoms[0] - B->Atoms[0];
	if (r < 0)
		return -1;
	else if (r > 0)
		return 1;
	else
		return 0;
}

// Comparison function w.r.t. second atom
int distAtom2(const void *BondA, const void *BondB)
{
	BOND *A = (BOND *)BondA,
		 *B = (BOND *)BondB;
	double r;
	r = A->Atoms[1] - B->Atoms[1];
	if (r < 0)
		return -1;
	else if (r > 0)
		return 1;
	else
		return 0;
}
