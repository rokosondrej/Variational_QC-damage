// build_diss: computes the dissipation part of the incremental energy
// of the system

#include <algorithm>
#include <math.h>
#include <stdio.h>

#ifdef _OPENMP
#include <omp.h>
#endif
#include "matrix.h"
#include "mex.h"

/******************* Definition of constants ******************/
const int NP = 128; // initial number of points for the trapezoidal rule
const int NR = 2;   // if relative error of trapezoidal rule is too large, use
                    // NP*NR points in next iterate

/******************** Function declarations *******************/
int evaluateD(double &D, const double eps, const double E, const double eps0,
              const double epsf, const double R,
              const double TOL); // numerical integration of D'

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

  // Test for Six input argument
  if (nrhs != 6)
    mexErrMsgTxt("Six input arguments required!\n");

  // Get the input data
  double *pr = mxGetPr(prhs[2]);
  double *prp = mxGetPr(prhs[3]);
  double TOL = mxGetScalar(prhs[4]);
  int maxNumThreads = (int)mxGetScalar(prhs[5]);

  // Compute the dissipation distance
  nlhs = 1;
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  double *prDiss = mxGetPr(plhs[0]);
  int i, status = 0;
  double Diss = 0;
  int NBonds = (int)mxGetNumberOfElements(prhs[1]);
  int name_R = mxGetFieldNumber(prhs[0], "R");
  int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
  int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
  {
    int _bond, _alpha, _beta, _status = 0;
    double _r1, _r2, _eps1, _eps2, _R, _D1, _D2, _D;
    double *_Atoms, *_Potential, *_Ra, *_Rb;
    double _Rab[2], _rab1[2], _rab2[2];

#pragma omp for reduction(+ \
                          : Diss)
    for (i = 0; i < NBonds; i++)
    {                // loop over all bonds
      _bond = i + 1; // MATLAB indexing

      // Get data for the bond
      _Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
      _alpha = (int)_Atoms[0];
      _beta = (int)_Atoms[1];
      _Potential =
          mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));

      // Get data for atoms associated with current bond
      _Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
      _Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
      _Rab[0] = _Rb[0] - _Ra[0];
      _Rab[1] = _Rb[1] - _Ra[1];
      _R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length

      // Previous time step parameters
      _rab1[0] = prp[2 * (_beta - 1)] - prp[2 * (_alpha - 1)];
      _rab1[1] = prp[2 * (_beta - 1) + 1] - prp[2 * (_alpha - 1) + 1];
      _r1 = sqrt(_rab1[0] * _rab1[0] + _rab1[1] * _rab1[1]); // deformed bond length
      _eps1 = (_r1 - _R) / _R;                               // bond strain

      // Current time step parameters
      _rab2[0] = pr[2 * (_beta - 1)] - pr[2 * (_alpha - 1)];
      _rab2[1] = pr[2 * (_beta - 1) + 1] - pr[2 * (_alpha - 1) + 1];
      _r2 = sqrt(_rab2[0] * _rab2[0] + _rab2[1] * _rab2[1]); // deformed bond length
      _eps2 = (_r2 - _R) / _R;                               // bond strain

      // Evaluate numerically integrals D(eps_i)
      if (_eps2 > _eps1)
      {
        _status += evaluateD(_D1, _eps1, _Potential[0], _Potential[1], _Potential[2], _R, TOL);
        _status += evaluateD(_D2, _eps2, _Potential[0], _Potential[1], _Potential[2], _R, TOL);
        _D = _D2 - _D1;

        // Allocate D part to the overall energy
        Diss += _D;
      }
    }

// Check convergence of _Di
#pragma omp atomic
    status += _status;
  }

  // Check convergence of _Di
  if (status > 0)
    mexWarnMsgTxt("Integration of Diss did not converge!");

  // Send the data back to MATLAB
  prDiss[0] = Diss;
}

/******************** Function definitions ********************/
// numerical integration of D'
int evaluateD(double &D, const double eps, const double E, const double eps0,
              const double epsf, const double R, const double TOL)
{
  double D1 = 0.0, D2 = 0.0, err, eps1, eps2, o1, o2, y1, y2;
  int i, N = NP, maxnp = NP * pow(NR, 8) + 1;
  int status = 0; // 0 for success
  D = 0.0;

  // Keep refining Simpson's discretization until convergence of eps
  if (eps > eps0)
  {
    err = 1 + TOL;
    while (
        (err > TOL) &
        (N <
         maxnp))
    { // adaptivity with respect to prescribed relative tolerance
      D2 = 0;
      eps1 = eps0;
      o1 = 1 - (eps0 / eps1) * exp(-(eps1 - eps0) / epsf);
      y1 = 0.5 * E * R * pow(eps1, 2);
      for (i = 0; i < N; i++)
      { // use the trapezoidal rule
        eps2 = eps1 + (eps - eps0) / N;
        o2 = 1 - (eps0 / eps2) * exp(-(eps2 - eps0) / epsf);
        y2 = 0.5 * E * R * pow(eps2, 2);
        D2 = D2 + 0.5 * (y1 + y2) * (o2 - o1);
        eps1 = eps2;
        o1 = o2;
        y1 = y2;
      }
      err = fabs(D2 - D1) / fabs(D2);
      N = N * NR;
      D1 = D2;
    }
    D = D2;
    if (err > TOL)
      status = 1; // test convergence of numerical integration; 1 for fail
  }
  else
  {
    D = 0;
  }

  // Return status
  return status;
}
