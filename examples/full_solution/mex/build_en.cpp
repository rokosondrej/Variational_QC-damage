// build_en: computes the elastic part of the incremental energy of
// the system

#include <stdio.h>
#include <math.h>
#include <algorithm>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mex.h"
#include "matrix.h"

/************************ Main program ************************/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Test for five input argument
	if (nrhs != 5)
		mexErrMsgTxt("Five input arguments required!\n");

	// Get the data
	double *promega = mxGetPr(prhs[2]);
	double *prx = mxGetPr(prhs[3]);
	int maxNumThreads = (int)mxGetScalar(prhs[4]);

	// Compute energy
	nlhs = 1;
	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *prEn = mxGetPr(plhs[0]);
	int i;
	double En = 0;
	int NBonds = (int)mxGetNumberOfElements(prhs[1]);
	int name_R = mxGetFieldNumber(prhs[0], "R");
	int name_Atoms = mxGetFieldNumber(prhs[1], "Atoms");
	int name_Potential = mxGetFieldNumber(prhs[1], "Potential");

#pragma omp parallel num_threads(maxNumThreads)
	{
		int _bond, _alpha, _beta;
		double _R, _r, _eps, _EnInt, _omega;
		double *_Ra, *_Rb, *_Atoms, *_Potential;
		double _Rab[2], _rab[2];

#pragma omp for reduction(+ \
						  : En)
		for (i = 0; i < NBonds; i++)
		{				   // loop over all bonds
			_bond = i + 1; // MATLAB indexing

			// Get data for the bond
			_Atoms = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Atoms));
			_alpha = (int)_Atoms[0];
			_beta = (int)_Atoms[1];
			_Potential = mxGetPr(mxGetFieldByNumber(prhs[1], _bond - 1, name_Potential));
			_omega = promega[_bond - 1]; // damage variable omega

			// Get data for atoms associated with current bond
			_Ra = mxGetPr(mxGetFieldByNumber(prhs[0], _alpha - 1, name_R));
			_Rb = mxGetPr(mxGetFieldByNumber(prhs[0], _beta - 1, name_R));
			_Rab[0] = _Rb[0] - _Ra[0];
			_Rab[1] = _Rb[1] - _Ra[1];
			_rab[0] = prx[2 * (_beta - 1)] - prx[2 * (_alpha - 1)];
			_rab[1] = prx[2 * (_beta - 1) + 1] - prx[2 * (_alpha - 1) + 1];
			_R = sqrt(_Rab[0] * _Rab[0] + _Rab[1] * _Rab[1]); // the initial bond length
			_r = sqrt(_rab[0] * _rab[0] + _rab[1] * _rab[1]); // deformed bond length

			/*<<<<<<<<<<<<<<<<<<<<<<<< Build energy >>>>>>>>>>>>>>>>>>>>>>>>*/
			// Compute phi
			_eps = (_r - _R) / _R; // bond strain
			if (_eps > 0)
			{
				_EnInt = (1 - _omega) * 0.5 * (_Potential[0] / _R) * (_r - _R) * (_r - _R);
			}
			else
			{
				_EnInt = 0.5 * (_Potential[0] / _R) * (_r - _R) * (_r - _R);
			}

			// Add to the overall energy
			En += _EnInt;
		}
	}

	// Send the data back to MATLAB
	prEn[0] = En;
}
