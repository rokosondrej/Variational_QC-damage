%% RUN_L_shaped: script for example in Section 5.1 (L-shaped plate example), full lattice solution

%% Clear workspace
clc; % clear command line
close all; % close all figures
matlabrc; % restore MATLAB path, etc.
path([pwd,'/mfiles'],path); % add path to mfiles folder containing all m-files
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
spparms('chmodsp') = 1;

%% Input variables

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 2;

% Geometry: construct an L-shaped region composed of bottom [0,SizeX1]x[0,SizeY1] and top [0,SizeX2]x[0,SizeY2] rectangles
dSize = 1;  % lattice spacing along x- and y-axes
SizeX1 = 32*dSize; % bottom rectanlge size along x-axis
SizeY1 = 32*dSize; % bottom rectanlge size along y-axis
SizeX2 = 64*dSize; % upper rectanlge size along x-axis, SizeX2 > SizeX1
SizeY2 = 32*dSize; % upper rectanlge size along y-axis

% Material: Potential = [E,eps0,epsf,-]
Potential = [1,0.1,0.25,0]; % for the domain
PotentialP = [1000,100,0.25,0]; % for stiff pad

% Stiff pad under DBC
Npad = 1/2;

% Tolerances
TOL_r = 1e-6; % elasticity solver relative tolerance
TOL_i = 1e-3; % relative tolerance for the numerical integration of D'
TOL_g = 1e-10; % geometric tolerance; a positive number < TOL is treated as zero

%% Construct data structures: atoms and bonds
atoms = build_atoms_L([SizeX1,SizeY1,SizeX2,SizeY2,dSize,dSize],maxNumThreads);
R0 = [atoms(:).R]';
bonds = build_bonds_L(atoms,[SizeX1,SizeY1,SizeX2,SizeY2,dSize,dSize],...
    Potential,Potential,TOL_g,maxNumThreads);

% Stiffen locally interactions connected to idPad atoms
idPad = find(abs(R0(1:2:end)-SizeX1-(SizeX2-SizeX1)/2)<dSize*Npad+TOL_g &...
    abs(R0(2:2:end)-SizeY1)<TOL_g);
idPadBonds = unique([atoms(idPad).BondList]);
for i = 1:length(idPadBonds)
    bonds(idPadBonds(i)).Potential = PotentialP;
end

%% Prescribe boundary conditions BC: 0 - no BC, 1 - Dirichlet BC type

% Prescribe vertical displacement at Gamma_3/2 atom
id1 = find(abs(R0(1:2:end)-SizeX1-(SizeX2-SizeX1)/2)<TOL_g &...
    abs(R0(2:2:end)-SizeY1)<TOL_g);
Fext = zeros(size(R0));
Fext(2*id1) = 1;

% Fix Gamma_1 part of the boundary
IDGamma_1 = find(R0(2:2:end)<TOL_g); % Gamma_1

% Construct C vector for CMOD control
C = zeros(size(R0));
id1 = (SizeX1/dSize+1)*(SizeY1/dSize);
id2 = (SizeX1/dSize+1)*(SizeY1/dSize+1)+1;
C(2*id1) = -1;
C(2*id2) = 1;

% Extract code numbers
DBCIndices = [2*IDGamma_1-1;2*IDGamma_1]; % code numbers for constrained atoms
DBCValues = 0*DBCIndices; % prescribed displacements for constrained atoms
FreeIndices = setdiff(1:2*length(atoms),DBCIndices)'; % free code numbers

%% Solve for the evolution process of the system
fprintf('Solving...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., CMOD = %g, %g s\n',0,0,...
    C'*R0,0);
timeIter = [0,0,0];
R = [R0,zeros(2*length(atoms),2)]; % r for all time steps
Z = sparse(length(bonds),3); % z for all time steps
i = 2;
Lambda = [0,0,0];

% Indirect displacement control constant
Larc = 0.025;

while C'*(R(:,i-1)-R0) < 16
    t_start_2 = tic;
    
    % MINIMIZE THE REDUCED ENERGY WITH RESPECT TO r
    [r2,Niter,Lambda(i)] = minimize_r(atoms,bonds,R(:,i-1),DBCIndices,...
        DBCValues,Fext,FreeIndices,R0,full(Z(:,i-1)),C,TOL_r,Larc,Lambda(i-1),maxNumThreads);
    
    % MINIMIZE WITH RESPECT TO z
    z2 = update_damage(atoms,bonds,r2,full(Z(:,i-1)),maxNumThreads);
    
    % Store converged minimizing values
    R(:,i) = r2;
    Z(:,i) = z2;
    
    % Print iteration message
    fprintf('%d step, %d Nwtn it., CMOD = %g, %g s\n',i,Niter,...
        C'*(R(:,i)-R0),toc(t_start_2));
    timeIter(i) = Niter;
    
    % Proceed to next time step
    i = i+1;
    
    % Plot system's current configuration
    figure(1),clf,hold all,axis equal;
    plot(r2(1:2:end),r2(2:2:end),'.k');
    drawnow;
end

% Print solution statistics
fprintf('\nE[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n',toc(t_start_1));

%% Draw results
% Plot atoms in deformed configuration
scale = 1;
step = length(Lambda);
figure(1),clf,hold all,axis equal,title(['Deformed structure, scale = ',...
    num2str(scale)]),xlabel('x'),ylabel('y');
plot(R0(1:2:end)+scale*(R(1:2:end,step)-R0(1:2:end)),...
    R0(2:2:end)+scale*(R(2:2:end,step)-R0(2:2:end)),'.k','MarkerSize',5);

% Compute reactions
REACT = zeros(size(R));
for i = 2:length(Lambda)
    REACT(:,i) = build_grad_r(atoms,bonds,R(:,i),full(Z(:,i)),maxNumThreads);
end

% Compute final energy evolutions
En = zeros(size(Lambda)); % elastic energy
DissDist = zeros(size(Lambda)); % dissipation distances
Wext = zeros(1,length(Lambda)); % work done by external forces (reactions)
for i = 2:length(Lambda)
    En(i) = build_en(atoms,bonds,full(Z(:,i)),R(:,i),maxNumThreads);
    DissDist(i) = build_diss(atoms,bonds,R(:,i),R(:,i-1),TOL_i,maxNumThreads);
    Wext(i) = Wext(i-1)+0.5*(Lambda(i-1)+Lambda(i))*...
        Fext'*(R(:,i)-R(:,i-1));
end
VarD = cumsum(DissDist); % dissipated energy

% Plot energy evolution paths
figure(2),clf,hold all,box on,xlabel('time step [-]'),...
    ylabel('Energy [kJ]'),title('MSPlastInterpolSumPlast');
plot(Lambda,En+VarD,'--k','linewidth',1);
plot(Lambda,En,'k');
plot(Lambda,VarD,'-.k');
plot(Lambda,Wext,':k');
legend('E+Var_D','E','Var_D','W_{ext}','location','northwest');

% Plot reaction force
figure(3),clf,hold all,box on,xlabel('\lambda(t)'),ylabel('F');
plot(Lambda,REACT(max(DBCIndices),:),'k');

%% Export data to Paraview
U = zeros(size(R0));
for istep = 1:length(Lambda)
    U(:,istep) = R(:,istep)-R0;
end
ParaView_export_2d('full_L',atoms,bonds,R0,U,Z,Lambda,maxNumThreads);
